package cn.itcast.mongo;

import cn.itcast.mongo.entity.User;
import cn.itcast.mongo.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongoDemoApplicationTests {

    @Autowired
    private UserRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void insert() {
        // 增或改
        User jack = repository.save(new User(1L, "Jack", 25));

        System.out.println("jack = " + jack);
    }

    @Test
    public void saveAll() {
        List<User> list = new ArrayList<>();
        list.add(new User(1L, "Amy", 16));
        list.add(new User(2L, "Lucy", 21));
        list.add(new User(3L, "Jack", 20));
        list.add(new User(4L, "Tom", 25));
        list.add(new User(5L, "John", 18));

        repository.saveAll(list);
    }

    @Test
    public void find() {
        // 根据id查询
        Optional<User> result = repository.findById(1L);
        // 从optional容器中取
        result.ifPresent(System.out::println);

        // 查询所有并排序
        List<User> list = repository.findAll(new Sort(Sort.Direction.ASC, "age"));
        for (User user : list) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void delete() {
        repository.deleteById(5L);
    }

    @Test
    public void findByCustom() {
        List<User> list = repository.queryByAgeLessThanAndNameLike(25, "y", Sort.by("age").descending());
        for (User user : list) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void testTemplate() {
        mongoTemplate.update(User.class)
                .matching(Query.query(Criteria.where("age").lt(25))) // {age: {$lt: 25}}
                // .apply(Update.update("age", 16)) // {$set : {age: 16}}
                .apply(new Update().inc("age", 1)) // {$inc : {age: 1}}
                .all();
    }
}
