package cn.itcast.mongo.repository;

import cn.itcast.mongo.entity.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author 虎哥
 */
public interface UserRepository extends MongoRepository<User, Long> {

    List<User> queryByAgeLessThanAndNameLike(Integer age, String name, Sort sort);
}
