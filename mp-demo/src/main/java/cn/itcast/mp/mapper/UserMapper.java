package cn.itcast.mp.mapper;

import cn.itcast.mp.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 虎哥
 */
public interface UserMapper extends BaseMapper<User> {
}
