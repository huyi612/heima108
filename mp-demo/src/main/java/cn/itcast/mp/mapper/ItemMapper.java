package cn.itcast.mp.mapper;

import cn.itcast.mp.entity.Item;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8(Item)表数据库访问层
 *
 * @author makejava
 * @since 2020-09-10 15:53:44
 */
public interface ItemMapper extends BaseMapper<Item> {
}