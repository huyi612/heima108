package cn.itcast.mp.controller;

import cn.itcast.mp.entity.Item;
import cn.itcast.mp.service.ItemService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8(Item)表控制层
 *
 * @author makejava
 * @since 2020-09-10 15:53:44
 */
@RestController
@RequestMapping("item")
public class ItemController {
    /**
     * 服务对象
     */
    @Resource
    private ItemService itemService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public Item selectOne(@PathVariable("id") Long id) {
        return this.itemService.getById(id);
    }

}