package cn.itcast.mp.service.impl;

import cn.itcast.mp.entity.Item;
import cn.itcast.mp.mapper.ItemMapper;
import cn.itcast.mp.service.ItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8(Item)表服务实现类
 *
 * @author makejava
 * @since 2020-09-10 15:53:44
 */
@Service
public class ItemServiceImpl extends ServiceImpl<ItemMapper, Item> implements ItemService {

}