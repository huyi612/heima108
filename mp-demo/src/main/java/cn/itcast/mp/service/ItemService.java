package cn.itcast.mp.service;

import cn.itcast.mp.entity.Item;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8(Item)表服务接口
 *
 * @author makejava
 * @since 2020-09-10 15:53:44
 */
public interface ItemService extends IService<Item> {
}