package cn.itcast.mp.service;

import cn.itcast.mp.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author 虎哥
 */
public interface UserService extends IService<User> {

}
