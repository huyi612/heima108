package cn.itcast.mp.service.impl;

import cn.itcast.mp.entity.User;
import cn.itcast.mp.mapper.UserMapper;
import cn.itcast.mp.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author 虎哥
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
