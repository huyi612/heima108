package cn.itcast.mp;

import cn.itcast.mp.entity.User;
import cn.itcast.mp.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MpDemoApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void contextLoads() {
        User user = userMapper.selectById(1L);
        System.out.println("user = " + user);

        List<User> list = userMapper.selectList(null);

        for (User user1 : list) {
            System.out.println("user1 = " + user1);
        }
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setUname("虎哥");
        user.setEmail("huge@itcast.cn");
        user.setAge(21);
        userMapper.insert(user);

        System.out.println("user = " + user);
    }

    @Test
    public void testQueryByIds() {
        List<User> list = userMapper.selectBatchIds(Arrays.asList(1L, 2L, 3L));
        for (User user : list) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(6L);
        user.setAge(18);

        userMapper.updateById(user);
    }

    @Test
    public void deleteById() {
        userMapper.deleteById(6L);
    }
}
