package cn.itcast.mp.service.impl;

import cn.itcast.mp.entity.User;
import cn.itcast.mp.service.UserService;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    public void testGet() {
        User user = userService.getById(1L);
        System.out.println("user = " + user);
    }

    @Test
    public void testGetWrapper() {
        // where id = 2
        Wrapper<User> queryWrapper = new QueryWrapper<User>().eq("id", "2");
        User one = userService.getOne(queryWrapper);
        System.out.println("one = " + one);
    }

    @Test
    public void testList() {
        List<User> list = userService.listByIds(Arrays.asList(3L, 4L, 5L));
        for (User user : list) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void listWrapper() {
        List<User> list = userService.list(
                new QueryWrapper<User>().le("age", "21").like("name", "a"));
        for (User user : list) {
            System.out.println("小鲜肉 ： " + user);
        }
    }

    @Test
    public void save() {
        User user = new User();
        user.setId(8L);
        user.setUname("虎妹");
        user.setAge(16);
        user.setEmail("HuMei@itcast.cn");
        userService.saveOrUpdate(user);

        System.out.println("user = " + user);
    }

    @Test
    public void saveAll() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(new User(null, "test_" + i, 20 + i, "test_" + i + "@itcast.cn"));
        }

        userService.saveBatch(list);
    }

    @Test
    public void update() {
        // 要更新的内容  update t_user set age = 18
        User user = new User();
        user.setAge(18);
        // 更新的条件  where id = 2
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>().eq("id", "2");
        // 执行
        userService.update(user, queryWrapper);
    }

    @Test
    public void testQuery() {
        // select * from t_user
        List<User> list = userService.query()
                // where age < 21 and name like "%a%"
                .le("age", 21).or().like("name", "a")
                // 查询结果
                .list();

        list.forEach(System.out::println);

    }

    @Test
    public void testUpdate(){
        // update t_user
        userService.update()
                // set age = 20
                .set("age", 20)
                // where id = 3
                .eq("id", "3")
                // 执行更新
                .update();
    }

    @Test
    public void testPageQuery(){
        // select * from t_user where age >= 18 limit 0,3
        Page<User> info = userService.query().ge("age", 18).page(new Page<>(4, 3));

        // info是分页结果，包含分页的各种信息
        System.out.println("总条数：" + info.getTotal());
        System.out.println("总页数：" + info.getPages());

        List<User> list = info.getRecords();

        list.forEach(System.out::println);

    }
}