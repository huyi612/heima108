package cn.itcast.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 虎哥
 */
@RestController
public class HelloController {

    @GetMapping("hello")
    public String hello() {
        return "hello, Spring boot!";
    }
}
