package cn.itcast.rabbitmq.spring;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 虎哥
 */
@Component
public class MqListener {

    @RabbitListener(queues = "heima.topic.queue1")
    public void listen(Map<String,Object> msg){
        System.out.println("消费者 接收到消息： " + msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "simple_queue", durable = "true"),
            exchange = @Exchange(name = "spring.topic.exchange", type = "topic"),
            key = "#"
    ))
    public void listen2(String msg){
        System.out.println("消费者2 接收到消息： " + msg);
    }
}
