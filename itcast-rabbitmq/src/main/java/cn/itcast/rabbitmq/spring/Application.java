package cn.itcast.rabbitmq.spring;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue springQueue() {
        return new Queue("spring.queue", true);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("spring.topic.exchange2", true, false);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder
                .bind(springQueue())
                .to(topicExchange())
                .with("#");
    }
}
