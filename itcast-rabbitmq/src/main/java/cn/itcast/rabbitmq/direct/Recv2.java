package cn.itcast.rabbitmq.direct;

import cn.itcast.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;

import static cn.itcast.rabbitmq.constants.NameConstants.DIRECT_EXCHANGE_NAME;
import static cn.itcast.rabbitmq.constants.NameConstants.DIRECT_QUEUE2_NAME;

/**
 * 消费者2
 */
public class Recv2 {

    public static void main(String[] argv) throws Exception {
        // 获取到连接
        Connection connection = ConnectionUtil.getConnection();
        // 获取通道
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(DIRECT_QUEUE2_NAME, false, false, false, null);
        
        // 绑定队列到交换机，同时指定需要订阅的routing key。订阅 insert、update、delete
        channel.queueBind(DIRECT_QUEUE2_NAME, DIRECT_EXCHANGE_NAME, "insert");
        channel.queueBind(DIRECT_QUEUE2_NAME, DIRECT_EXCHANGE_NAME, "update");
        channel.queueBind(DIRECT_QUEUE2_NAME, DIRECT_EXCHANGE_NAME, "delete");

        // 定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties,
                    byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body);
                System.out.println(" [消费者2] received : " + msg + "!");
            }
        };
        // 监听队列，自动ACK
        channel.basicConsume(DIRECT_QUEUE2_NAME, true, consumer);
    }
}