package cn.itcast.rabbitmq.constants;

/**
 * @author 虎哥
 */
public class NameConstants {
    public final static String SIMPLE_QUEUE_NAME = "heima.simple.queue";
    public final static String WORK_QUEUE_NAME = "heima.work.queue";
    public final static String FANOUT_QUEUE1_NAME = "heima.fanout.queue1";
    public final static String FANOUT_QUEUE2_NAME = "heima.fanout.queue2";
    public final static String DIRECT_QUEUE1_NAME = "heima.direct.queue1";
    public final static String DIRECT_QUEUE2_NAME = "heima.direct.queue2";
    public final static String TOPIC_QUEUE1_NAME = "heima.topic.queue1";
    public final static String TOPIC_QUEUE2_NAME = "heima.topic.queue2";
    public final static String FANOUT_EXCHANGE_NAME = "heima.fanout.exchange";
    public final static String DIRECT_EXCHANGE_NAME = "heima.direct.exchange";
    public final static String TOPIC_EXCHANGE_NAME = "heima.topic.exchange";

    public final static String SPRING_QUEUE1_NAME = "spring.queue1";
    public final static String SPRING_QUEUE2_NAME = "spring.queue2";

    public final static String SPRING_EXCHANGE_NAME = "spring.topic";
}
