package cn.itcast.starter.es.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Arrays;
import java.util.List;

/**
 * @author 虎哥
 */
@ConfigurationProperties(prefix = "heima.elastic")
public class ElasticProperties {

    private List<String> hosts;
    private String indexName;

    public ElasticProperties() {
        this.hosts = Arrays.asList("http://localhost:9200");
    }

    public List<String> getHosts() {
        return hosts;
    }

    public void setHosts(List<String> hosts) {
        this.hosts = hosts;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }
}
