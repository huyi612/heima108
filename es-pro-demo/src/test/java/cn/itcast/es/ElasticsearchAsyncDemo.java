package cn.itcast.es;

import org.apache.http.HttpHost;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * @author 虎哥
 */
public class ElasticsearchAsyncDemo {

    private RestHighLevelClient highLevelClient;

    @Test
    public void testGetAsync() throws IOException, InterruptedException {
        // 1.创建一个Request对象
        GetRequest request = new GetRequest("goods", "1");

        // 2.发出请求
        highLevelClient.getAsync(request, RequestOptions.DEFAULT, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse response) {
                // 成功回调
                String json = response.getSourceAsString();
                System.out.println("json = " + json);
            }

            @Override
            public void onFailure(Exception e) {
                // 失败回调
            }
        });

        // 3.请求已经发出
        System.out.println("请求已经发出 = ");

        Thread.sleep(2000);
    }

    @Test
    public void testSearchAsync() throws InterruptedException {
        String prefix = "s";

        // 1.准备Request对象
        SearchRequest request = new SearchRequest("goods");

        // 2.准备请求参数
        // 2.1.创建SourceBuilder
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 2.2.添加一个suggest查询
        sourceBuilder.suggest(
                new SuggestBuilder().addSuggestion("name_suggest",
                        SuggestBuilders.completionSuggestion("name").prefix(prefix)));
        request.source(sourceBuilder);

        // 3.发出请求
        highLevelClient.searchAsync(request, RequestOptions.DEFAULT, new ActionListener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse response) {
                // 4.解析结果
                Suggest suggest = response.getSuggest();
                // 4.1.根据suggest的名称获取suggestion结果，注意结果要向下转型
                CompletionSuggestion suggestion = suggest.getSuggestion("name_suggest");
                // 4.2.获取补全的结果options
                List<CompletionSuggestion.Entry.Option> options = suggestion.getOptions();
                // 4.3.遍历
                System.out.println(prefix + "的补全词条有：");
                for (CompletionSuggestion.Entry.Option option : options) {
                    Text text = option.getText();
                    System.out.println("text = " + text);
                }
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        System.out.println("请求发出，任务结束！");

        Thread.sleep(2000);
    }

    @Before
    public void beforeClass() {
        highLevelClient = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://ly-es:9200")
        ));
    }

    @After
    public void afterClass() throws IOException {
        highLevelClient.close();
    }
}
