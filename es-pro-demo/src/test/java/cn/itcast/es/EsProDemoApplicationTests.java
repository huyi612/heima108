package cn.itcast.es;

import cn.itcast.es.pojo.Goods;
import cn.itcast.es.repository.GoodsRepository;
import com.leyou.starter.elastic.dto.PageInfo;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EsProDemoApplicationTests {

    @Autowired
    private GoodsRepository repository;

    @Test
    public void createIndex() {
        repository.createIndex(ElasticsearchBasicDemo.SOURCE);
    }

    @Test
    public void deleteIndex() {
        repository.deleteIndex();
    }

    @Test
    public void addDocument() {
        Goods goods = new Goods(1L, Arrays.asList("红米9", "手机"), "红米9手机 数码", 1499L);
        repository.save(goods);
    }

    @Test
    public void deleteById() {
        repository.deleteById(1L);
    }

    @Test
    public void saveAll() {
        // 1.准备文档数据
        List<Goods> list = new ArrayList<>();
        list.add(new Goods(1L, Arrays.asList("红米9", "手机"), "红米9手机 数码", 1499L));
        list.add(new Goods(2L, Arrays.asList("三星", "Galaxy", "手机"), "三星 Galaxy A90 手机 数码 疾速5G 骁龙855", 3099L));
        list.add(new Goods(3L, Arrays.asList("Sony", "WH-1000XM3"), "Sony WH-1000XM3 降噪耳机 数码", 2299L));
        list.add(new Goods(4L, Arrays.asList("松下", "剃须刀"), "松下电动剃须刀高转速磁悬浮马达", 599L));
        list.add(new Goods(5L, Arrays.asList("索尼", "剃须刀"), "索尼电动剃须刀高转速磁悬浮马达", 699L));

        repository.saveAll(list);
    }

    @Test
    public void getById() throws InterruptedException {
        Mono<Goods> mono = repository.queryById(1L);

        mono.subscribe(System.out::println);

        Thread.sleep(2000);
    }

    @Test
    public void suggestTest() throws InterruptedException {
        Mono<List<String>> mono = repository.suggestBySingleField("name", "s");
        mono.subscribe(System.out::println);
        Thread.sleep(2000);
    }

    @Test
    public void search() throws InterruptedException {
        // 2.给Request对象准备请求参数
        // 2.1.创建SourceBuilder
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 2.2.query条件
        // 2.2.1.构建布尔查询
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        // 2.2.2.添加must
        queryBuilder.must(QueryBuilders.matchQuery("title", "手机电动剃须刀耳机"));
        // 2.2.3.添加filter
        queryBuilder.filter(QueryBuilders.rangeQuery("price").gt(100).lt(10000));
        sourceBuilder.query(queryBuilder);
        // 2.3.分页条件
        sourceBuilder.from(3).size(3);
        // 2.4.排序
        sourceBuilder.sort("price", SortOrder.ASC);
        // 2.5.高亮
        sourceBuilder.highlighter(new HighlightBuilder().field("title"));
        // 2.6.source过滤
        sourceBuilder.fetchSource(new String[]{"id", "title", "price"}, null);


        Mono<PageInfo<Goods>> mono = repository.queryBySourceBuilderForPageHighlight(sourceBuilder);

        mono.subscribe(info -> {
            // 总条数
            long total = info.getTotal();
            System.out.println("total = " + total);
            // 数据
            List<Goods> list = info.getContent();
            list.forEach(System.out::println);
        });

        Thread.sleep(2000);
    }
}
