package cn.itcast.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsProDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsProDemoApplication.class, args);
    }

}
