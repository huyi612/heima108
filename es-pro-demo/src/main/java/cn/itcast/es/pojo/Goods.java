package cn.itcast.es.pojo;

import com.leyou.starter.elastic.annotaions.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 虎哥
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Goods {
    @Id
    private Long id;
    private List<String> name;
    private String title;
    private Long price;
}