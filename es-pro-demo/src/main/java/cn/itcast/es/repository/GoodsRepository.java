package cn.itcast.es.repository;

import cn.itcast.es.pojo.Goods;
import com.leyou.starter.elastic.repository.Repository;

/**
 * @author 虎哥
 */
public interface GoodsRepository extends Repository<Goods, Long> {
}
