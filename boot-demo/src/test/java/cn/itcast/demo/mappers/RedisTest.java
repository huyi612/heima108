package cn.itcast.demo.mappers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void findById() {
        System.out.println("redisTemplate = " + redisTemplate);
        // Jedis 中的所有方法都是 redis中的命令的名称


        // 获取字符串操作对象
        ValueOperations<String, String> valueOperation = redisTemplate.opsForValue();
        valueOperation.set("num", "123");

        System.out.println("num: " + valueOperation.get("num"));

        // 获取hash操作对象
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();

        hashOperations.put("user1", "name", "Rose");
        hashOperations.put("user1", "age", "18");

        Map<String,String> map = hashOperations.entries("user1");
        System.out.println("map = " + map);
    }
}