package cn.itcast.demo.web;

import cn.itcast.demo.config.PaymentProperties;
import cn.itcast.demo.service.AccountService;
import cn.itcast.demo.entity.Account;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("bank")
public class BankController {

    private final PaymentProperties paymentProperties;
    private final AccountService accountService;

    public BankController(PaymentProperties paymentProperties, AccountService accountService) {
        this.paymentProperties = paymentProperties;
        this.accountService = accountService;
    }

    /**
     * 转账业务
     * @param from 转出账户id
     * @param to 转入账户id
     * @param amount 转出金额
     * @return 结果
     */
    @PutMapping("transfer")
    public String transfer(
            @RequestParam("from") Long from, @RequestParam("to") Long to, @RequestParam("amount") Long amount){
        // 调用service业务
        accountService.transfer(from, to, amount);
        // 返回结果
        return "转账成功";
    }

    @GetMapping("/{id}")
    public Account findById(@PathVariable("id") Long id) {
        return accountService.findById(id);
    }

    /**
     * 页面跳转
     */
    @GetMapping("/list")
    public ModelAndView findAll() {
        // 查询数据
        List<Account> list =  accountService.findAll();
        // 准备mv
        ModelAndView mv = new ModelAndView("list");
        // 模型
        mv.addObject("list", list);
        return mv;
    }

    @GetMapping("hello")
    public PaymentProperties hello() {
        return paymentProperties;
    }

    @GetMapping("hello2")
    public PaymentProperties hello2() {
        return paymentProperties;
    }
}
