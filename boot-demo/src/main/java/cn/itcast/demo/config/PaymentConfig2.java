package cn.itcast.demo.config;

/**
 * 注解@Configuration声明当前类是一个配置类，代替以前的xml
 * @author 虎哥
 */
// @Configuration
public class PaymentConfig2 {
    /**
     * 注册一个PaymentProperties类的对象到Spring的容器中，返回值对象会注入到spring容器
     * 方法名称就是这个bean的ID
     * @return PaymentProperties
     */
    //@Bean
    //@ConfigurationProperties(prefix = "pay.nopassword")
    public PaymentProperties paymentProperties(){
        return new PaymentProperties();
    }
}
