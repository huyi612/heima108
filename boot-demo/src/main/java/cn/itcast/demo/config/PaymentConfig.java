package cn.itcast.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

/**
 * 注解@Configuration声明当前类是一个配置类，代替以前的xml
 * @author 虎哥
 */
// @Configuration
public class PaymentConfig {

    @Value("${pay.nopassword.maxAmountPerDeal}")
    public long maxAmountPerDeal;

    @Value("${pay.nopassword.maxAmountTotal}")
    public long maxAmountTotal;
    /**
     * 注册一个PaymentProperties类的对象到Spring的容器中，返回值对象会注入到spring容器
     * 方法名称就是这个bean的ID
     * @return PaymentProperties
     */
    @Bean
    public PaymentProperties paymentProperties(){
        PaymentProperties paymentProperties = new PaymentProperties();
        paymentProperties.setMaxAmountPerDeal(maxAmountPerDeal);
        paymentProperties.setMaxAmountTotal(maxAmountTotal);
        return paymentProperties;
    }
}
