package cn.itcast.demo.config;

import cn.itcast.demo.interceptors.MyHandlerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 虎哥
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 添加拦截器
        registry.addInterceptor(new MyHandlerInterceptor())
                // 添加拦截的路径
                .addPathPatterns("/bank/**")
                // 排除不想拦截的路径
                .excludePathPatterns("/bank/hello2");
    }


}
