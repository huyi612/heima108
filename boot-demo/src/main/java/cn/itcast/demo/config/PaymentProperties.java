package cn.itcast.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 虎哥
 */
@Data
@Component
@ConfigurationProperties(prefix = "pay.nopassword")
public class PaymentProperties {

    private int loadOnStartUp = -1;
    /**
     * 单笔交易的最大限额，单位是分
     */
    private long maxAmountPerDeal;

    /**
     * 每日交易总量的最大限额
     */
    private long maxAmountTotal;

    private User user = new User();

    @Data
    public static class User {
        private String name;
        private int age;
        private List<User> girls;
    }
}