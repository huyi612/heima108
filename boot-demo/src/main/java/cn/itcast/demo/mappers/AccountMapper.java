package cn.itcast.demo.mappers;

import cn.itcast.demo.entity.Account;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 虎哥
 */
public interface AccountMapper {

    Account findById(Long id);

    void updateMoney(@Param("id")Long id, @Param("amount") Long amount);

    @Select("select * from tb_account")
    List<Account> findAll();
}
