package cn.itcast.demo.service.impl;

import cn.itcast.demo.service.AccountService;
import cn.itcast.demo.entity.Account;
import cn.itcast.demo.mappers.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 虎哥
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public Account findById(Long id) {
        return accountMapper.findById(id);
    }

    @Override
    @Transactional
    public void transfer(Long from, Long to, Long amount) {
        // 转入
        accountMapper.updateMoney(to, amount);
        // 转出
        accountMapper.updateMoney(from, -amount);
    }

    @Override
    public List<Account> findAll() {
        return accountMapper.findAll();
    }
}
