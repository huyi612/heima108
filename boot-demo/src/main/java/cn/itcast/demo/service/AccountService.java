package cn.itcast.demo.service;

import cn.itcast.demo.entity.Account;

import java.util.List;

/**
 * @author 虎哥
 */
public interface AccountService {

    Account findById(Long id);

    void transfer(Long from, Long to, Long amount);

    List<Account> findAll();

}
