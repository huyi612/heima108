package cn.itcast.demo.entity;

import lombok.Data;

/**
 * @author 虎哥
 */
@Data
public class Account {
    private Long id;
    private String accountName;
    private Long money;
}