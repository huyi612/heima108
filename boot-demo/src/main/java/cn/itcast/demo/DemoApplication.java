package cn.itcast.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 虎哥
 */
@MapperScan(basePackages = "cn.itcast.demo.mappers")
@SpringBootApplication
public class DemoApplication {
    public static void main(String[] args) {
        // 初始化spring容器并允许
        SpringApplication.run(DemoApplication.class, args);
    }
}
