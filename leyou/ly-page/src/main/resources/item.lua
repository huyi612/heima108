-- 导入common模块
local common = require("common")
local read_redis = common.read_redis
local read_http = common.read_http
-- 导入cjson模块
local cjson = require("cjson")

-- 导入模板渲染模块
local template = require("resty.template")

-- 获取spuId
local spuId = ngx.var.spuId;

-- redis配置
local ip = "127.0.0.1"
local port = 6379
-- 查询spu
local key = "page:spu:id:"..spuId
local spuInfoJSON = read_redis(ip, port, key)
if not spuInfoJSON then
    ngx.log(ngx.err, "query spu in redis fail，change to Http，spuId: ", spuId)
    -- redis查询失败，改查http到tomcat
    spuInfoJSON = read_http("/spu/"..spuId, nil)
    if not spuInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end
-- 查询sku
local key = "page:sku:id:"..spuId
local skuInfoJSON = read_redis(ip, port, key)
if not skuInfoJSON then
    ngx.log(ngx.err, "query sku in redis fail，change to Http，spuId: ", spuId)
    -- redis查询失败，改查http到tomcat
    skuInfoJSON = read_http("/sku/"..spuId, nil)
    if not skuInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end
-- 查询sku
local key = "page:detail:id:"..spuId
local detailInfoJSON = read_redis(ip, port, key)
if not detailInfoJSON then
    ngx.log(ngx.err, "query detail in redis fail，change to Http，spuId: ", spuId)
    -- redis查询失败，改查http到tomcat
    detailInfoJSON = read_http("/detail/"..spuId, nil)
    if not detailInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end

-- 把spu反序列化
local spuInfo = cjson.decode(spuInfoJSON)
local categoryIds = spuInfo.categoryIds
-- 查询商品分类
local key = "page:category:id:"..categoryIds[3]
local categoryInfoJSON = read_redis(ip, port, key)
if not categoryInfoJSON then
    ngx.log(ngx.err, "query category in redis fail，change to Http，spuId: ", spuId)
    -- redis查询失败，改查http到tomcat
    categoryInfoJSON = read_http("/categories", {ids=table.concat(categoryIds, ",")})
    if not categoryInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end

-- 查询 brand
local key = "page:brand:id:"..spuInfo.brandId
local brandInfoJSON = read_redis(ip, port, key)
if not brandInfoJSON then
    ngx.log(ngx.err, "query brand in redis fail，change to Http，brandId: ", spuInfo.brandId)
    -- redis查询失败，改查http到tomcat
    brandInfoJSON = read_http("/brand/"..spuInfo.brandId, nil)
    if not brandInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end

-- 查询 spec
local key = "page:spec:id:"..categoryIds[3]
local specInfoJSON = read_redis(ip, port, key)
if not specInfoJSON then
    ngx.log(ngx.err, "query spec in redis fail，change to Http，category: ", categoryIds[3])
    -- redis查询失败，改查http到tomcat
    specInfoJSON = read_http("/spec/"..categoryIds[3], nil)
    if not specInfoJSON then
        -- http查询失败，说明商品不存在！
        return ngx.exit(404)
    end
end
-- 数据
local data = {
    skuList = skuInfoJSON,
    name = spuInfo.name,
    categories = categoryInfoJSON,
    brand = brandInfoJSON,
    detail = detailInfoJSON,
    specs = specInfoJSON,

}
-- 渲染
template.render("item.html", data);