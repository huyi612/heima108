package com.leyou.page.vo;

import lombok.Data;

import javax.persistence.*;

/**
 * @author 虎哥
 */
@Data
@Table(name = "tb_sku")
public class SkuVO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "spu_id")
    private Long spuId;
}