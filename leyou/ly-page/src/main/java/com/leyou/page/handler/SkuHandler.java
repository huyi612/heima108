package com.leyou.page.handler;

import com.leyou.page.service.GoodsPageService;
import com.leyou.page.vo.SkuVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.annotation.CanalTable;
import top.javatool.canal.client.handler.EntryHandler;

/**
 * @author 虎哥
 */
@CanalTable(value = "tb_sku")
@Component
public class SkuHandler implements EntryHandler<SkuVO> {

    @Autowired
    private GoodsPageService pageService;

    @Override
    public void insert(SkuVO skuDTO) {
        pageService.loadSkuListData(skuDTO.getSpuId());
    }

    @Override
    public void update(SkuVO before, SkuVO after) {
        pageService.loadSkuListData(after.getSpuId());
    }

    @Override
    public void delete(SkuVO skuDTO) {
        pageService.deleteSkuList(skuDTO.getSpuId());
    }
}