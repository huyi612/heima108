package com.leyou.page.handler;

import com.leyou.page.service.GoodsPageService;
import com.leyou.page.vo.BrandVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.javatool.canal.client.handler.EntryHandler;

/**
 * @author 虎哥
 */
@Slf4j
@Component
public class BrandHandler implements EntryHandler<BrandVO> {

    @Autowired
    private GoodsPageService pageService;

    @Override
    public void insert(BrandVO brandVO) {
        // 重写brand到redis
        pageService.saveBrandVO(brandVO);
        log.info("品牌新增，已经重写redis");
    }

    @Override
    public void update(BrandVO before, BrandVO after) {
        // 重写brand到redis
        pageService.saveBrandVO(after);
        log.info("品牌更新，已经重写redis");
    }

    @Override
    public void delete(BrandVO brandVO) {
        // 删除品牌
        pageService.deleteBrandById(brandVO.getId());
        log.info("品牌删除，已经清理redis");
    }
}
