package com.leyou.auth.util;

import com.leyou.auth.dto.Payload;
import com.leyou.auth.dto.UserDetails;
import org.junit.Test;

public class JwtUtilsTest {

    private JwtUtils jwtUtils = new JwtUtils("afdafsafasdagdagadgdagdagdagf");

    @Test
    public void createJwt() {


        String jwt = jwtUtils.createJwt(UserDetails.of(110L, "ergouzi"));

        System.out.println("jwt = " + jwt);
    }

    @Test
    public void parseJWT() {
        String jwt = "eyJhbGciOiJIUzM4NCJ9.eyJqdGkiOiI0YzU2YzdkOGU2ZjI0OTU3YWFlYmVlNjhlOTcwYjI2ZCIsInVzZXIiOiJ7XCJpZFwiOjExMCxcInVzZXJuYW1lXCI6XCJqYWNrIG1hXCJ9IiwiZXhwIjoxNjAxMDAxNjE2fQ==.MT97sP0InzDEb6zXDdeasm9dJyVKffjktaeyj4Ze0B4A8iufeyx-KwPseOV4RETb";

        Payload payload = jwtUtils.parseJwt(jwt);

        System.out.println("payload = " + payload);
    }
}