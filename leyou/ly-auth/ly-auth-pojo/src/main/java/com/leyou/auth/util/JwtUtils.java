package com.leyou.auth.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.leyou.auth.constants.RedisConstants;
import com.leyou.auth.dto.Payload;
import com.leyou.auth.dto.UserDetails;
import com.leyou.common.exceptions.LyException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.leyou.auth.constants.RedisConstants.JTI_KEY_PREFIX;

/**
 * @author 虎哥
 */
public class JwtUtils {

    /**
     * JWT解析器
     */
    private final JwtParser jwtParser;
    /**
     * 秘钥
     */
    private final SecretKey secretKey;

    private final static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private StringRedisTemplate redisTemplate;

    public JwtUtils(String key) {
        // 生成秘钥
        secretKey = Keys.hmacShaKeyFor(key.getBytes(Charset.forName("UTF-8")));
        // JWT解析器
        this.jwtParser = Jwts.parserBuilder().setSigningKey(secretKey).build();
    }

    /**
     * 生成jwt，用默认的JTI
     *
     * @param userDetails 用户信息
     * @return JWT
     */
    public String createJwt(UserDetails userDetails) {
        return createJwt(userDetails, RedisConstants.TOKEN_EXPIRE_SECONDS);
    }

    /**
     * 生成jwt，自己指定的过期时间
     *
     * @param userDetails 用户信息
     * @return JWT
     */
    public String createJwt(UserDetails userDetails, int expireSeconds) {
        try {
            // 生成JTI
            String jti = createJti();
            // 生成token
            String jwt = Jwts.builder().signWith(secretKey)
                    .setId(jti)
                    .claim("user", mapper.writeValueAsString(userDetails))
                    .setExpiration(DateTime.now().plusSeconds(expireSeconds).toDate())
                    .compact();
            // 写redis
            redisTemplate.opsForValue().set(
                    JTI_KEY_PREFIX + userDetails.getId(), jti, expireSeconds, TimeUnit.SECONDS);
            return jwt;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 验证并解析jwt，返回包含用户信息的载荷
     *
     * @param jwt token
     * @return 载荷，包含JTI和用户信息
     */
    public Payload parseJwt(String jwt) {
        try {
            // 解析JWT中的数据
            Jws<Claims> claimsJws = jwtParser.parseClaimsJws(jwt);
            // 获取载荷
            Claims claims = claimsJws.getBody();
            // 组装Payload
            Payload payload = new Payload();
            payload.setJti(claims.getId());
            payload.setUserDetail(mapper.readValue(claims.get("user", String.class), UserDetails.class));
            // 获取Redis中的JTI
            String cacheJTI = redisTemplate.opsForValue()
                    .get(JTI_KEY_PREFIX + payload.getUserDetail().getId());
            // 判断是否为空
            if (StringUtils.isEmpty(cacheJTI)) {
                throw new LyException(401, "登录超时！");
            }
            // 判断JTI是否一致
            if (!cacheJTI.equals(payload.getJti())) {
                // JTI不同，应该在其它设备登录了
                throw new LyException(401, "您的账号已经在其它设备登录，如果不是本人操作，请及时修改密码！");
            }
            return payload;
        } catch (IllegalArgumentException e) {
            throw new LyException(401, "用户未登录！");
        } catch (JwtException e) {
            throw new LyException(401, "登录无效或者已经超时！");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String createJti() {
        return StringUtils.replace(UUID.randomUUID().toString(), "-", "");
    }

    public void refreshJwt(String jwt) {
        // 解析
        Payload payload = parseJwt(jwt);
        // 刷新
        redisTemplate.expire(JTI_KEY_PREFIX + payload.getUserDetail().getId(),
                RedisConstants.TOKEN_EXPIRE_SECONDS, TimeUnit.SECONDS);
    }

    public void deleteJwt(Long userId) {
        redisTemplate.delete(JTI_KEY_PREFIX + userId);
    }
}