package com.leyou.auth.constants;

/**
 * @author 虎哥
 */
public class RedisConstants {
    /**
     * 用户登录时记录TokenID的key前缀
     */
    public static final String JTI_KEY_PREFIX = "auth:login:uid:";
    /**
     * 用户登录时记录TokenID的超时时间
     */
    public static final int TOKEN_EXPIRE_SECONDS = 1800;
}