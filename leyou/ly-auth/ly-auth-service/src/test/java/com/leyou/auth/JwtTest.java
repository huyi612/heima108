package com.leyou.auth;

import com.leyou.auth.dto.Payload;
import com.leyou.auth.dto.UserDetails;
import com.leyou.auth.util.JwtUtils;
import com.leyou.user.client.UserClient;
import com.leyou.user.dto.UserDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 虎哥
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtTest {

    @Autowired
    private JwtUtils jwtUtils;

    @Test
    public void testJwt() {
        String jwt = jwtUtils.createJwt(UserDetails.of(120L, "Rose"));
        System.out.println("jwt = " + jwt);

        Payload payload = jwtUtils.parseJwt(jwt);
        System.out.println("payload = " + payload);
    }
    
    @Autowired
    private UserClient userClient;

    @Test
    public void queryUser() {
        UserDTO user = userClient.queryUserByUsernameAndPassword("lisi", "1234");
        System.out.println("user = " + user);
    }
}
