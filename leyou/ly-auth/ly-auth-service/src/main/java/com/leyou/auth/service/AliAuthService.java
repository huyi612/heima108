package com.leyou.auth.service;

import com.leyou.auth.dto.AliOssSignatureDTO;

/**
 * @author 虎哥
 */
public interface AliAuthService {
    AliOssSignatureDTO generateOssSignature();
}
