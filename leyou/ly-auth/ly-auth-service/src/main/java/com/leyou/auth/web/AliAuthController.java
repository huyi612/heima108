package com.leyou.auth.web;

import com.leyou.auth.dto.AliOssSignatureDTO;
import com.leyou.auth.service.AliAuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("ali")
public class AliAuthController {

    private final AliAuthService aliAuthService;

    public AliAuthController(AliAuthService aliAuthService) {
        this.aliAuthService = aliAuthService;
    }

    /**
     * 申请OSS的上传的策略和签名
     * @return 策略和签名信息
     */
    @GetMapping("/oss/signature")
    public ResponseEntity<AliOssSignatureDTO> getOssSignature(){
        // 生成签名
        AliOssSignatureDTO aliOssSignatureDTO = aliAuthService.generateOssSignature();
        // 返回
        return ResponseEntity.ok(aliOssSignatureDTO);
    }
}
