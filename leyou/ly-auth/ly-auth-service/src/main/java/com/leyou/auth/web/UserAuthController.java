package com.leyou.auth.web;

import com.leyou.auth.service.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("user")
public class UserAuthController {

    @Autowired
    private UserAuthService userAuthService;

    /**
     * 登录
     *
     * @param username 用户名
     * @param password 密码
     * @param response response对象
     * @return 无
     */
    @PostMapping("login")
    public ResponseEntity<Void> login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            HttpServletResponse response) {
        userAuthService.login(username, password, response);
        return ResponseEntity.noContent().build();
    }

    /**
     * 退出登录
     * @param request 请求对象
     * @param response 响应对象
     * @return 无
     */
    @PostMapping("logout")
    public ResponseEntity<Void> logout(HttpServletRequest request, HttpServletResponse response){
        userAuthService.logout(request, response);
        return ResponseEntity.noContent().build();
    }
}
