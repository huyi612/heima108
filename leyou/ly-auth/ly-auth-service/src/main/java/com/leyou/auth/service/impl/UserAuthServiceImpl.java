package com.leyou.auth.service.impl;

import com.leyou.auth.dto.Payload;
import com.leyou.auth.dto.UserDetails;
import com.leyou.auth.service.UserAuthService;
import com.leyou.auth.util.JwtUtils;
import com.leyou.common.exceptions.LyException;
import com.leyou.common.utils.CookieUtils;
import com.leyou.user.client.UserClient;
import com.leyou.user.dto.UserDTO;
import feign.FeignException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.leyou.auth.constants.JwtConstants.COOKIE_NAME;
import static com.leyou.auth.constants.JwtConstants.DOMAIN;

/**
 * @author 虎哥
 */
@Service
public class UserAuthServiceImpl implements UserAuthService {

    @Autowired
    private UserClient userClient;

    @Autowired
    private JwtUtils jwtUtils;

    @Override
    public void login(String username, String password, HttpServletResponse response) {
        // 1.根据用户名和密码去查询用户
        UserDTO user = null;
        try {
            user = userClient.queryUserByUsernameAndPassword(username, password);
        } catch (FeignException e) {
            throw new LyException(e.status(), e.contentUTF8());
        }
        if(user == null){
            throw new LyException(400, "用户名或密码错误");
        }

        // 2.判断查询结果，如果正确，需要生成JWT
        String jwt = jwtUtils.createJwt(UserDetails.of(user.getId(), user.getUsername()));

        // 3.把jwt写入cookie
        writeCookie(response, jwt);
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        // 1.获取cookie中的JWT
        String jwt = CookieUtils.getCookieValue(request, COOKIE_NAME);
        if(StringUtils.isBlank(jwt)){
            // 本来就没有登录，无需任何操作
            return;
        }
        // 2.解析JWT，获取用户id
        Payload payload = jwtUtils.parseJwt(jwt);
        Long userId = payload.getUserDetail().getId();

        // 3.删除redis中的用户信息
        jwtUtils.deleteJwt(userId);

        // 4.删除cookie
        CookieUtils.deleteCookie(COOKIE_NAME, DOMAIN, response);
    }

    private void writeCookie(HttpServletResponse response, String jwt) {
        Cookie cookie = new Cookie(COOKIE_NAME, jwt);
        cookie.setMaxAge(-1); // 浏览器关闭，cookie消失
        cookie.setPath("/"); // domain下的一切路径，都可以携带cookie
        cookie.setDomain(DOMAIN);
        cookie.setHttpOnly(true);// 禁止JS操作Cookie，防范XSS攻击
        response.addCookie(cookie);
    }
}
