package com.leyou.auth.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.auth.entity.ClientInfo;
import com.leyou.auth.mapper.ClientMapper;
import com.leyou.auth.service.ClientService;
import com.leyou.common.exceptions.LyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author 虎哥
 */
@Service
public class ClientServiceImpl extends ServiceImpl<ClientMapper, ClientInfo> implements ClientService {

    @Value("${ly.jwt.key}")
    private String key;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public String getJwtKey(String clientId, String secret) {
        // 1.根据clientId查询ClientInfo信息
        ClientInfo info = query().eq("client_id", clientId).one();
        // 2.判断查询是否成功
        if (info == null) {
            // id错误
            throw new LyException(401, "客户端身份错误！我要报警了！");
        }
        // 3.比较secret
        if(!passwordEncoder.matches(secret, info.getSecret())){
            // 密码错误
            throw new LyException(401, "客户端身份错误！我要报警了！");
        }
        // 4.返回秘钥
        return key;
    }
}