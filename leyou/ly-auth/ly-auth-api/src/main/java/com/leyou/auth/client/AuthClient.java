package com.leyou.auth.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 虎哥
 */
@FeignClient("auth-service")
public interface AuthClient {

    /**
     * 申请JWT的秘钥
     * @param clientId 客户端id
     * @param secret 客户端密码
     * @return JWT秘钥
     */
    @GetMapping("/client/key")
    String getJwtKey(@RequestParam("clientId") String clientId, @RequestParam("secret") String secret);
}
