package com.leyou.auth.config;

import com.leyou.auth.util.JwtUtils;
import com.leyou.auth.interceptors.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author 虎哥
 */
@ConditionalOnProperty(prefix = "ly.auth", name = "enable", havingValue = "true")
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Lazy
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private ClientProperties properties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new LoginInterceptor(jwtUtils));
        // 获取要拦截的路径
        List<String> includePathPatterns = properties.getIncludePathPatterns();
        if (!CollectionUtils.isEmpty(includePathPatterns)) {
            // 路径不为空
            registration.addPathPatterns(includePathPatterns);
        }
        // 获取放行的路径
        List<String> excludePathPatterns = properties.getExcludePathPatterns();
        if (!CollectionUtils.isEmpty(excludePathPatterns)) {
            // 路径不为空
            registration.excludePathPatterns(excludePathPatterns);
        }
    }
}
