package com.leyou.auth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author 虎哥
 */
@Data
@ConfigurationProperties(prefix = "ly.auth")
public class ClientProperties {
    private String clientId;
    private String secret;
    private Boolean enable = false;
    private List<String> includePathPatterns;
    private List<String> excludePathPatterns;
}
