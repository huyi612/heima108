package com.leyou.auth.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * @author 虎哥
 */
@EnableFeignClients(basePackages = "com.leyou.auth.client")
@Configuration
public class FeignConfig {

}
