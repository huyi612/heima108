package com.leyou.auth.utils;

import com.leyou.auth.dto.UserDetails;

/**
 * @author 虎哥
 */
public class UserContext {
    /**
     * 存放用户身份信息
     */
    private static final ThreadLocal<UserDetails> tl = new ThreadLocal<>();
    /**
     * 存放用户的JWT信息
     */
    private static final ThreadLocal<String> jwtTL = new ThreadLocal<>();

    public static void setUser(UserDetails user) {
        tl.set(user);
    }

    public static UserDetails getUser() {
        return tl.get();
    }

    public static void removeUser() {
        tl.remove();
    }
    public static void setJWT(String jwt) {
        jwtTL.set(jwt);
    }

    public static String getJWT() {
        return jwtTL.get();
    }

    public static void removeJWT() {
        jwtTL.remove();
    }
}
