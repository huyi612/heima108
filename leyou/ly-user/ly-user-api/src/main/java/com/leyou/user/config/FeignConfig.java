package com.leyou.user.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

/**
 * @author 虎哥
 */
@EnableFeignClients(basePackages = "com.leyou.user.client")
@Configuration
public class FeignConfig {

}
