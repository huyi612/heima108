package com.leyou.search.client;

import com.leyou.item.client.ItemClient;
import com.leyou.item.dto.CategoryDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ItemClientTest {

    @Autowired
    private ItemClient itemClient;

    @Test
    public void queryCategoryById() {
        CategoryDTO categoryDTO = itemClient.queryCategoryById(2L);
        Assert.assertNotNull(categoryDTO);
        System.out.println("categoryDTO = " + categoryDTO);

        CategoryDTO dto = new RestTemplate().getForObject("http://localhost:8081/category/1", CategoryDTO.class);
    }

    @Test
    public void testBrand() {
        // ResponseEntity<BrandDTO> responseEntity = itemClient.queryBrandById(1115L);

//        int status = responseEntity.getStatusCodeValue();
//        System.out.println("status = " + status);
//        BrandDTO brandDTO = responseEntity.getBody();
//        System.out.println("brandDTO = " + brandDTO);
    }
}