package com.leyou.search.constants;

/**
 * @author 虎哥
 */
public abstract class SearchConstants {
    public static final String INDEX_SOURCE = "{\n" +
            "  \"settings\": {\n" +
            "    \"number_of_shards\": 3,\n" +
            "    \"number_of_replicas\": 1, \n" +
            "    \"analysis\": {\n" +
            "      \"analyzer\": {\n" +
            "        \"my_pinyin\": {\n" +
            "          \"tokenizer\": \"ik_max_word\",\n" +
            "          \"filter\": [\n" +
            "            \"py\"\n" +
            "          ]\n" +
            "        }\n" +
            "      },\n" +
            "      \"filter\": {\n" +
            "        \"py\": {\n" +
            "          \"type\": \"pinyin\",\n" +
            "          \"keep_full_pinyin\": false,\n" +
            "          \"keep_joined_full_pinyin\": true,\n" +
            "          \"keep_original\": true,\n" +
            "          \"limit_first_letter_length\": 16,\n" +
            "          \"none_chinese_pinyin_tokenize\": false\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  },\n" +
            "  \"mappings\": {\n" +
            "    \"properties\": {\n" +
            "      \"id\": {\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"suggestion\": {\n" +
            "        \"type\": \"completion\",\n" +
            "        \"analyzer\": \"my_pinyin\",\n" +
            "        \"search_analyzer\": \"ik_max_word\"\n" +
            "      },\n" +
            "      \"title\":{\n" +
            "        \"type\": \"text\",\n" +
            "        \"analyzer\": \"my_pinyin\",\n" +
            "        \"search_analyzer\": \"ik_max_word\"\n" +
            "      },\n" +
            "      \"image\":{\n" +
            "        \"type\": \"keyword\",\n" +
            "        \"index\": false\n" +
            "      },\n" +
            "      \"specs\":{\n" +
            "        \"type\": \"nested\",\n" +
            "        \"properties\": {\n" +
            "          \"name\":{\"type\":\"keyword\"},\n" +
            "          \"value\":{\"type\":\"keyword\"}\n" +
            "        }\n" +
            "      },\n" +
            "      \"updateTime\":{\n" +
            "        \"type\": \"date\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public final static String SUGGEST_FIELD = "suggestion";
    public final static String SEARCH_FIELD = "title";
    public final static String PRE_TAG = "<am>";
    public final static String POST_TAG = "</am>";
    public final static String[] SHOW_FIELD = new String[]{"id", "title", "image", "prices", "sold"};
}
