package com.leyou.search.service.impl;

import com.leyou.common.dto.PageDTO;
import com.leyou.common.exceptions.LyException;
import com.leyou.item.client.ItemClient;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.dto.SpuDTO;
import com.leyou.search.constants.SearchConstants;
import com.leyou.search.dto.SearchParamDTO;
import com.leyou.search.entity.Goods;
import com.leyou.search.repository.GoodsRepository;
import com.leyou.search.service.SearchService;
import com.leyou.starter.elastic.dto.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;

import static com.leyou.search.constants.SearchConstants.*;

/**
 * @author 虎哥
 */
@Slf4j
@Service
public class SearchServiceImpl implements SearchService {

    private final GoodsRepository repository;
    private final ItemClient itemClient;

    public SearchServiceImpl(GoodsRepository repository, ItemClient itemClient) {
        this.repository = repository;
        this.itemClient = itemClient;
    }

    @Override
    public void createIndexAndMapping() {
        try {
            // 删除索引库
            repository.deleteIndex();
        } catch (Exception e) {
            log.warn("索引库不存在，无需删除！", e);
        }
        // 创建索引库
        repository.createIndex(SearchConstants.INDEX_SOURCE);
    }

    @Override
    public void loadData() {
        int page = 1, rows = 50;
        log.info("开始数据导入：");
        while (true) {
            // 查询spu信息
            PageDTO<SpuDTO> info = itemClient.querySpuByPage(page, rows, true, null, null, null);
            log.info("开始导入第{}页数据", page);
            // 取出spu
            List<Goods> goodsList = info.getItems()
                    .stream()
                    // 把查询到的SPU信息封装为一个Goods
                    .map(this::buildGoods)
                    .collect(Collectors.toList());
            // 把goods写入索引库
            repository.saveAll(goodsList);
            log.info("导入第{}页数据完成！", page);
            // 翻页
            page++;
            // 判断是否是最后一页
            if (page > info.getTotalPage()) {
                break;
            }
        }
        log.info("导入数据完成！");
    }

    @Override
    public Mono<List<String>> getSuggestion(String key) {
        return repository.suggestBySingleField(SUGGEST_FIELD, key);
    }

    @Override
    public Mono<PageInfo<Goods>> search(SearchParamDTO param) {
        // 1.搜索条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 2.组织条件
        String key = param.getKey();
        if(StringUtils.isBlank(key)){
            // 没有搜索条件
            throw new LyException(400, "搜索关键字不能为空！");
        }
        // 2.1.query
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        // 关键字搜索
        queryBuilder.must(QueryBuilders.matchQuery(SEARCH_FIELD, key));
        // filter过滤
        Map<String, String> filters = param.getFilters();
        if(!CollectionUtils.isEmpty(filters)){
            // queryBuilder.filter()
            System.out.println("filters = " + filters);
        }
        sourceBuilder.query(queryBuilder);
        
        // 2.2.排序
        String sortBy = param.getSortBy();
        if(StringUtils.isNotBlank(sortBy)) {
            sourceBuilder.sort(sortBy, param.getDesc() ? SortOrder.DESC : SortOrder.ASC);
        }

        // 2.3.分页
        sourceBuilder.from(param.getFrom()).size(param.getSize());

        // 2.4.高亮
        sourceBuilder.highlighter(new HighlightBuilder().field(SEARCH_FIELD).preTags(PRE_TAG).postTags(POST_TAG));

        // 2.5.source过滤
        sourceBuilder.fetchSource(SHOW_FIELD, null);

        // 3.分页搜索
        return repository.queryBySourceBuilderForPageHighlight(sourceBuilder);
    }

    @Override
    public void saveGoodsById(Long spuId) {
        // 根据id查询Spu
        SpuDTO spu = itemClient.queryGoodsById(spuId);
        // 构建goods
        Goods goods = buildGoods(spu);
        // 新增
        repository.save(goods);
    }

    @Override
    public void deleteGoodsById(Long spuId) {
        repository.deleteById(spuId);
    }

    private Goods buildGoods(SpuDTO spu) {
        Long spuId = spu.getId();
        if (spuId == null) {
            throw new LyException(400, "商品id不能为空！");
        }

        // 1.商品标题，用于搜索，标题 + 分类 + 品牌
        String title = spu.getTitle() + spu.getBrandName() + spu.getCategoryName();

        // 2.自动补全的候选字段，可以包含多个值，例如分类名称、品牌名称、商品名称
        Set<String> suggestion = new HashSet<>();
        // 2.1.获取分类名称
        String categoryName = spu.getCategoryName();
        Collections.addAll(suggestion, StringUtils.split(categoryName, "/"));
        // 2.2.品牌名称
        suggestion.add(spu.getBrandName());
        // 2.3.商品名称
        suggestion.add(spu.getName());

        // 3.规格参数的key和value对，用于过滤  [{name:"", value:""},{name:"", value:""}]
        // 这里的name是规格参数的名称，这里的value是规格参数的值
        List<Map<String, Object>> specs = new ArrayList<>();
        // 3.1.查询规格参数的键值对
        List<SpecParamDTO> params = itemClient.querySpecsValues(spuId, true);
        // 3.2.数据转换
        for (SpecParamDTO param : params) {
            Map<String, Object> map = new HashMap<>();
            map.put("name", param.getName());
            map.put("value", chooseSegment(param));
            specs.add(map);
        }

        // 4.查询sku
        List<SkuDTO> skuList = spu.getSkus();
        if(CollectionUtils.isEmpty(skuList)) {
            skuList = itemClient.querySkuBySpuId(spuId);
        }
        // 4.1.销量，把所有sku的销量累加
        long sold = skuList.stream().mapToLong(SkuDTO::getSold).sum();
        // 4.2.价格
        Set<Long> prices = skuList.stream().map(SkuDTO::getPrice).collect(Collectors.toSet());
        // 4.3.图片
        String image = StringUtils.substringBefore(skuList.get(0).getImages(), ",");

        // 创建Goods对象
        Goods goods = new Goods();
        // 赋值
        goods.setUpdateTime(new Date());
        goods.setTitle(title);
        goods.setSuggestion(suggestion);
        goods.setSpecs(specs);
        goods.setSold(sold);
        goods.setPrices(prices);
        goods.setImage(image);
        goods.setId(spuId);
        goods.setCategoryId(spu.getCid3());
        goods.setBrandId(spu.getBrandId());
        return goods;
    }

    private Object chooseSegment(SpecParamDTO p) {
        Object value = p.getValue();
        if (value == null || StringUtils.isBlank(value.toString())) {
            return "其它";
        }
        if (!p.getNumeric() || StringUtils.isBlank(p.getSegments()) || value instanceof Collection) {
            return value;
        }
        double val = parseDouble(value.toString());
        String result = "其它";
        // 保存数值段
        for (String segment : p.getSegments().split(",")) {
            String[] segs = segment.split("-");
            // 获取数值范围
            double begin = parseDouble(segs[0]);
            double end = Double.MAX_VALUE;
            if (segs.length == 2) {
                end = parseDouble(segs[1]);
            }
            // 判断是否在范围内
            if (val >= begin && val < end) {
                if (segs.length == 1) {
                    result = segs[0] + p.getUnit() + "以上";
                } else if (begin == 0) {
                    result = segs[1] + p.getUnit() + "以下";
                } else {
                    result = segment + p.getUnit();
                }
                break;
            }
        }
        return result;
    }

    private double parseDouble(String str) {
        try {
            return Double.parseDouble(str);
        } catch (Exception e) {
            return 0;
        }
    }
}
