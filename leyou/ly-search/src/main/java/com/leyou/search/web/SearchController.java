package com.leyou.search.web;

import com.leyou.search.dto.SearchParamDTO;
import com.leyou.search.entity.Goods;
import com.leyou.search.service.SearchService;
import com.leyou.starter.elastic.dto.PageInfo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("goods")
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    /**
     * 初始化并导入数据
     * @return 无
     */
    @GetMapping("init")
    public ResponseEntity<String> init(){
        searchService.createIndexAndMapping();
        searchService.loadData();
        return ResponseEntity.ok("初始化完成！");
    }

    /**
     * 根据关键字自动补全提示
     * @param key 关键字
     * @return 提示的词条列表
     */
    @GetMapping("suggestion")
    public Mono<List<String>> getSuggestion(@RequestParam("key") String key){
        return searchService.getSuggestion(key);
    }

    /**
     * 搜索商品
     * @param param 请求参数
     * @return 分页结果
     */
    @PostMapping("/list")
    public Mono<PageInfo<Goods>> search(@RequestBody SearchParamDTO param){
        return searchService.search(param);
    }

}
