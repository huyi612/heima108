package com.leyou.search.repository;

import com.leyou.search.entity.Goods;
import com.leyou.starter.elastic.repository.Repository;

/**
 * @author 虎哥
 */
public interface GoodsRepository extends Repository<Goods, Long> {
}
