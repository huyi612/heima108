package com.leyou.common.advice;

import com.leyou.common.exceptions.LyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 类上添加@ControllerAdvice注解的作用是声明当前类会作用在所有的添加了@Controller注解的类的方法上
 * @author 虎哥
 */
@ControllerAdvice
public class CommonExceptionAdvice {

    /**
     * 所有的controller方法抛出RuntimeException.class异常，都会被当前方法处理
     * 并且异常信息会作为参数传递给当前方法
     * @param e 异常信息
     * @return 错误的提示
     */
    @ExceptionHandler(LyException.class)
    public ResponseEntity<String> handleException(LyException e){
        return ResponseEntity.status(e.getStatus()).body(e.getMessage());
    }
}
