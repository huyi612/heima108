package com.leyou.common.annotations;

import com.leyou.common.advice.CommonExceptionAdvice;
import com.leyou.common.advice.CommonLogAdvice;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 虎哥
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({CommonExceptionAdvice.class, CommonLogAdvice.class})
public @interface EnableExceptionAdvice {
}
