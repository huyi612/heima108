package com.leyou.common.exceptions;

import lombok.Getter;

/**
 * @author 虎哥
 */
@Getter
public class LyException extends RuntimeException{

    private int status;

    public LyException(int status, String message) {
        super(message);
        this.status = status;
    }

    public LyException(int status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public LyException(int status, Throwable cause) {
        super(cause);
        this.status = status;
    }
}
