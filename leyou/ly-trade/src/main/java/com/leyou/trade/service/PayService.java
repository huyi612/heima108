package com.leyou.trade.service;

import java.util.Map;

/**
 * @author 虎哥
 */
public interface PayService {
    String getPayUrl(Long id);

    void handleWxNotify(Map<String, String> data);
}
