package com.leyou.trade.web;

import com.leyou.trade.entity.CartItem;
import com.leyou.trade.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    private CartService cartService;
    /**
     * 添加购物车条目
     * @param cartItem 购物车条目数据
     * @return 无
     */
    @PostMapping
    public ResponseEntity<Void> addCartItem(@RequestBody CartItem cartItem){
        cartService.addCartItem(cartItem);
        // 新增成功，返回201
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 批量添加购物车条目
     * @param cartItems 购物车条目数据
     * @return 无
     */
    @PostMapping("list")
    public ResponseEntity<Void> addCartItem(@RequestBody List<CartItem> cartItems){
        cartService.addCartItems(cartItems);
        // 新增成功，返回201
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 更新购物车商品数量
     * @param skuId 商品id
     * @param num 数量
     * @return 无
     */
    @PutMapping
    public ResponseEntity<Void> updateCartItem(@RequestParam("id") Long skuId, @RequestParam("num") Integer num){
        cartService.updateCartItem(skuId, num);
        // 更新成功，返回204
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * 查询购物车列表
     * @return 购物车集合
     */
    @GetMapping("list")
    public ResponseEntity<List<CartItem>> queryCartList(){
        return ResponseEntity.ok(cartService.queryCartList());
    }
}
