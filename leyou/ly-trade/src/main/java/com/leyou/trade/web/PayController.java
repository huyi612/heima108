package com.leyou.trade.web;

import com.leyou.trade.dto.WxResult;
import com.leyou.trade.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("pay")
public class PayController {

    @Autowired
    private PayService payService;

    /**
     * 下单并获取支付链接
     * @param id 订单id
     * @return 支付链接
     */
    @GetMapping("/url/{id}")
    public ResponseEntity<String> getPayUrl(@PathVariable("id") Long id) {
        String url = payService.getPayUrl(id);
        return ResponseEntity.ok(url);
    }

    /**
     * 微信异步通知的接口
     * @return 无
     */
    @PostMapping(value = "/wx/notify", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<WxResult> handleWxNotify(@RequestBody Map<String,String> data){
        payService.handleWxNotify(data);
        return ResponseEntity.ok(new WxResult());
    }
}
