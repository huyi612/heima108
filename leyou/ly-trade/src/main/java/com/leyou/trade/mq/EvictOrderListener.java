package com.leyou.trade.mq;

import com.leyou.trade.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.leyou.common.constants.MQConstants.QueueConstants.EVICT_ORDER_QUEUE;

/**
 * @author 虎哥
 */
@Component
public class EvictOrderListener {

    @Autowired
    private OrderService orderService;

    /**
     * 监听超时订单
     * @param orderId 订单id
     */
    @RabbitListener(queues = EVICT_ORDER_QUEUE)
    public void listenOverdueOrder(Long orderId){
        orderService.evictOverdueOrder(orderId);
    }
}
