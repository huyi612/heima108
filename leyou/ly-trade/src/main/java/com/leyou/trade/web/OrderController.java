package com.leyou.trade.web;

import com.leyou.trade.dto.OrderDTO;
import com.leyou.trade.dto.OrderFormDTO;
import com.leyou.trade.entity.Order;
import com.leyou.trade.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 创建订单
     *
     * @param orderFormDTO 订单信息
     * @return 订单id
     */
    @PostMapping
    public ResponseEntity<Long> createOrder(@RequestBody OrderFormDTO orderFormDTO) {
        Long orderId = orderService.createOrder(orderFormDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(orderId);
    }

    /**
     * 根据id查询订单
     *
     * @param id 订单id
     * @return 订单
     */
    @GetMapping("{id}")
    public ResponseEntity<OrderDTO> queryOrderById(@PathVariable("id") String id) {
        Order order = orderService.getById(id);
        return ResponseEntity.ok(new OrderDTO(order));
    }

    /**
     * 查询订单状态
     * @param id 订单id
     * @return 订单状态
     */
    @GetMapping("/status/{id}")
    public ResponseEntity<Integer> queryOrderStatus(@PathVariable("id") String id) {
        return ResponseEntity.ok(orderService.queryOrderStatus(id));
    }

}
