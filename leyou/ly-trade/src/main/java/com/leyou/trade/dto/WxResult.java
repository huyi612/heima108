package com.leyou.trade.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

/**
 * @author 虎哥
 */
@Data
@JacksonXmlRootElement(localName = "xml")
public class WxResult {
    @JacksonXmlProperty(localName = "return_code")
    private String returnCode = "SUCCESS";
    @JacksonXmlProperty(localName = "return_msg")
    private String returnMsg = "OK";
}
