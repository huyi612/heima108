package com.leyou.trade.repository;

import com.leyou.trade.entity.CartItem;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author 虎哥
 */
public interface CartItemRepository extends MongoRepository<CartItem, String> {

    List<CartItem> queryByUserId(Long userId, Sort sort);
}
