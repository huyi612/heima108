package com.leyou.trade.interceptors;

import com.leyou.auth.utils.UserContext;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author 虎哥
 */
@Component
public class FeignTokenInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        String jwt = UserContext.getJWT();
        if (StringUtils.isNotBlank(jwt)) {
            template.header("Cookie", "LY_TOKEN=" + jwt);
        }
    }
}
