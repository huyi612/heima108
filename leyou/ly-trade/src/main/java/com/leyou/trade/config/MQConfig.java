package com.leyou.trade.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.leyou.common.constants.MQConstants.ExchangeConstants.DEAD_EXCHANGE_NAME;
import static com.leyou.common.constants.MQConstants.ExchangeConstants.ORDER_EXCHANGE_NAME;
import static com.leyou.common.constants.MQConstants.QueueConstants.DEAD_ORDER_QUEUE;
import static com.leyou.common.constants.MQConstants.QueueConstants.EVICT_ORDER_QUEUE;
import static com.leyou.common.constants.MQConstants.RoutingKeyConstants.EVICT_ORDER_KEY;

/**
 * @author 虎哥
 */
@Configuration
public class MQConfig {

    @Bean
    public TopicExchange normalOrderExchange(){
        return new TopicExchange(ORDER_EXCHANGE_NAME, true, false);
    }

    @Bean
    public Queue deadOrderQueue(){
        return QueueBuilder.durable(DEAD_ORDER_QUEUE)
                .withArgument("x-message-ttl", 10000)
                .withArgument("x-dead-letter-exchange", DEAD_EXCHANGE_NAME)
                .build();
    }

    @Bean
    public Binding deadQueueBinding(){
        return BindingBuilder
                .bind(deadOrderQueue())
                .to(normalOrderExchange())
                .with(EVICT_ORDER_KEY);
    }


    @Bean
    public TopicExchange deadOrderExchange(){
        return new TopicExchange(DEAD_EXCHANGE_NAME, true, false);
    }

    @Bean
    public Queue evictOrderQueue(){
        return new Queue(EVICT_ORDER_QUEUE, true);
    }

    @Bean
    public Binding evictQueueBinding(){
        return BindingBuilder
                .bind(evictOrderQueue())
                .to(deadOrderExchange())
                .with(EVICT_ORDER_KEY);
    }

    @Bean
    public Jackson2JsonMessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}
