package com.leyou.trade.utils;

import com.leyou.auth.dto.UserDetails;
import com.leyou.auth.utils.UserContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 虎哥
 */
@Component
public class CollectionNameBuilder {
    /**
     * 集合名称的前缀
     */
    @Value("${ly.mongo.collectionNamePrefix}")
    private String namePrefix;

    public String build(){
        // 获取当前用户
        UserDetails user = UserContext.getUser();
        if (user == null) {
            return "";
        }
        // 用一个固定collection名前缀，拼接上用户id计算出的数字，作为collection名
        return namePrefix + user.getId().hashCode() % 100;
    }
}