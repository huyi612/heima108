package com.leyou.trade.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderFormDTO {
    /**
     * 收获人地址id
     */
    private Long addressId;
    /**
     * 付款类型
     */
    private Integer paymentType;
    /**
     * 订单中商品, key是skuId，value是购买数量
     */
    private Map<Long,Integer> carts;
}