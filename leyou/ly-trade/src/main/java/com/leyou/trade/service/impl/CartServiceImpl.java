package com.leyou.trade.service.impl;

import com.leyou.auth.utils.UserContext;
import com.leyou.common.exceptions.LyException;
import com.leyou.trade.entity.CartItem;
import com.leyou.trade.repository.CartItemRepository;
import com.leyou.trade.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author 虎哥
 */
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartItemRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void addCartItem(CartItem cartItem) {
        // 获取当前登录的用户
        Long userId = UserContext.getUser().getId();
        // 组装购物车条目的id
        String cartId = createId(userId, cartItem.getSkuId());
        // 先查询当前购物车商品
        Optional<CartItem> optional = repository.findById(cartId);
        Integer newNum = cartItem.getNum();
        // 判断是否存在
        if(optional.isPresent()){
            // 存在,修改数量
            cartItem = optional.get();
            cartItem.setNum(cartItem.getNum() + newNum);
        }
        // 填充字段
        cartItem.setId(cartId);
        cartItem.setUserId(userId);
        cartItem.setUpdateTime(new Date());
        // 写入MongoDB
        repository.save(cartItem);
    }

    @Override
    public void addCartItems(List<CartItem> cartItems) {
        for (CartItem cartItem : cartItems) {
            addCartItem(cartItem);
        }
    }

    @Override
    public List<CartItem> queryCartList() {
        // 获取用户id
        Long userId = UserContext.getUser().getId();
        // 根据用户id查询购物车列表
        List<CartItem> cartItems = repository.queryByUserId(userId, Sort.by("updateTime").descending());
        if(CollectionUtils.isEmpty(cartItems)){
            throw new LyException(404, "购物车为空！");
        }
        return cartItems;
    }

    @Override
    public void updateCartItem(Long skuId, Integer num) {
        // 获取用户id
        Long userId = UserContext.getUser().getId();
        // 更新num
        mongoTemplate.update(CartItem.class)
                // where id = id
                .matching(Query.query(Criteria.where("_id").is(createId(userId, skuId))))
                // set num = num
                .apply(Update.update("num", num))
                // 更新匹配到的第一个
                .first();
    }



    private String createId(Long userId, Long skuId) {
        return String.format("u%ds%d", userId, skuId);
    }
}
