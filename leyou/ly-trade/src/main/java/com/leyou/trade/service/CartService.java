package com.leyou.trade.service;

import com.leyou.trade.entity.CartItem;

import java.util.List;

/**
 * @author 虎哥
 */
public interface CartService {
    void addCartItem(CartItem cartItem);

    List<CartItem> queryCartList();

    void updateCartItem(Long skuId, Integer num);

    void addCartItems(List<CartItem> cartItems);

}
