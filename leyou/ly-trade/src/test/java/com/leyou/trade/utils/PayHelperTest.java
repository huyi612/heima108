package com.leyou.trade.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PayHelperTest {

    @Autowired
    private PayHelper payHelper;

    @Test
    public void getPayUrl() {
        String url = payHelper.getPayUrl(48510150150513L, 1L, "乐优商城会员充值");

        System.out.println("url = " + url);
    }
}