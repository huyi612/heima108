package com.leyou.item.dto;

import com.leyou.common.dto.BaseDTO;
import com.leyou.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 虎哥
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class CategoryDTO extends BaseDTO {
    private Long id;
    private String name;
    private Long parentId;
    private Boolean isParent;
    private Integer sort;

    public CategoryDTO(BaseEntity entity) {
        super(entity);
    }

    public static <T extends BaseEntity> List<CategoryDTO> convertEntityList(Collection<T> entityList) {
        if(CollectionUtils.isEmpty(entityList)){
            // 如果集合为空
            return Collections.emptyList();
        }
        return entityList.stream().map(CategoryDTO::new).collect(Collectors.toList());
        /*
        // 创建一个DTO的集合
        List<CategoryDTO> list = new ArrayList<>(entityList.size());
        // 遍历PO集合
        for (BaseEntity baseEntity : entityList) {
            // 把PO封装为DTO，并加入集合中
            list.add(new CategoryDTO(baseEntity));
        }
        // 返回集合
        return list;
        */
    }
}
