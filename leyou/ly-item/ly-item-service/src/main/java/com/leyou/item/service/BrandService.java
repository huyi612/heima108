package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.common.dto.PageDTO;
import com.leyou.item.dto.BrandDTO;
import com.leyou.item.entity.Brand;

import java.util.List;

/**
 * @author 虎哥
 */
public interface BrandService extends IService<Brand> {
    void saveBrand(BrandDTO brandDTO);
    /**
     * 分页查询品牌
     * @param page 当前页
     * @param rows 每页大小
     * @param key 搜索关键字
     * @return 分页结果
     */
    PageDTO<BrandDTO> queryBrandByPage(Integer page, Integer rows, String key);

    /**
     * 更新品牌
     * @param brandDTO 品牌的DTO
     */
    void updateBrand(BrandDTO brandDTO);

    /**
     * 根据分类id查询品牌
     * @param id 分类id
     * @return 品牌集合
     */
    List<BrandDTO> queryBrandByCategory(Long id);
}