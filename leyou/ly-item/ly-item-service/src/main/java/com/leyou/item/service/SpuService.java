package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.common.dto.PageDTO;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.dto.SpuDTO;
import com.leyou.item.entity.Spu;

import java.util.List;

/**
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8(Spu)表服务接口
 *
 * @author makejava
 * @since 2020-09-15 16:37:20
 */
public interface SpuService extends IService<Spu> {
    PageDTO<SpuDTO> querySpuByPage(Long brandId, Long categoryId, Long id, Integer page, Integer rows, Boolean saleable);

    void saveGoods(SpuDTO spuDTO);

    void updateSaleable(Long spuId, Boolean saleable);

    SpuDTO queryGoodsById(Long id);

    void updateGoods(SpuDTO spuDTO);

    List<SpecParamDTO> querySpecValue(Long spuId, Boolean searching);
}