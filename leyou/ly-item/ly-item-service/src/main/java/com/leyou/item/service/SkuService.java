package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.entity.Sku;

import java.util.List;
import java.util.Map;

/**
 * @author 虎哥
 */
public interface SkuService extends IService<Sku> {
    List<SkuDTO> querySkuBySpuId(Long spuId);

    void deductStock(Map<Long, Integer> skuMap);

    void plusStock(Map<Long, Integer> skuMap);
}
