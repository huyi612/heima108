package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leyou.item.entity.Spu;

/**
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8(Spu)表数据库访问层
 *
 * @author makejava
 * @since 2020-09-15 16:37:20
 */
public interface SpuMapper extends BaseMapper<Spu> {
}