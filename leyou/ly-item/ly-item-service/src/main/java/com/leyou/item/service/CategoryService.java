package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.dto.CategoryDTO;
import com.leyou.item.entity.Category;

import java.util.List;

/**
 * @author 虎哥
 */
public interface CategoryService extends IService<Category> {
    List<CategoryDTO> queryCategoryByBrand(Long brandId);
}
