package com.leyou.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.common.exceptions.LyException;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.entity.SpecParam;
import com.leyou.item.mapper.SpecParamMapper;
import com.leyou.item.service.SpecParamService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 虎哥
 */
@Service
public class SpecParamServiceImpl extends ServiceImpl<SpecParamMapper, SpecParam> implements SpecParamService {
    @Override
    public List<SpecParamDTO> queryParams(Long categoryId, Long groupId, Boolean searching) {
        // 健壮性判断：两个条件不能都为null
        if(categoryId == null && groupId == null){
            throw new LyException(400, "请求参数不能为空！");
        }
        // SELECT * FROM tb_spec_param WHERE category_id = 105 AND group_id = 1 AND searching = true
        List<SpecParam> list = query()
                .eq(categoryId != null, "category_id", categoryId)
                .eq(groupId != null, "group_id", groupId)
                .eq(searching != null, "searching", searching)
                .list();
        // 转换DTO
        return SpecParamDTO.convertEntityList(list);
    }
}