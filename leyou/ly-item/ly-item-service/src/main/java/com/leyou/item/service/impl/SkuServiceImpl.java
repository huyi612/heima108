package com.leyou.item.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.leyou.common.exceptions.LyException;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.entity.Sku;
import com.leyou.item.mapper.SkuMapper;
import com.leyou.item.service.SkuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 虎哥
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements SkuService {

    private static final String DEDUCT_STOCK_STATEMENT = "com.leyou.item.mapper.SkuMapper.deductStock";
    @Override
    public List<SkuDTO> querySkuBySpuId(Long spuId) {
        List<Sku> list = query().eq("spu_id", spuId).list();
        return SkuDTO.convertEntityList(list);
    }

    @Override
    @Transactional
    public void deductStock(Map<Long, Integer> skuMap) {
        try {
            this.executeBatch(sqlSession -> {
                for (Map.Entry<Long, Integer> entry : skuMap.entrySet()) {
                    // 封装为map
                    Map<String,Object> param = new HashMap<>();
                    param.put("id", entry.getKey());
                    param.put("num", entry.getValue());
                    // 更新库存，指定statement的ID和参数
                    sqlSession.update(DEDUCT_STOCK_STATEMENT, param);
                }
                // 刷新，提交SQL
                sqlSession.flushStatements();
            });
        } catch (Exception e) {
            throw new LyException(500, "库存不足！");
        }
    }

    @Override
    public void plusStock(Map<Long, Integer> skuMap) {
        try {
            this.executeBatch(sqlSession -> {
                for (Map.Entry<Long, Integer> entry : skuMap.entrySet()) {
                    // 封装为map
                    Map<String,Object> param = new HashMap<>();
                    param.put("id", entry.getKey());
                    param.put("num", -entry.getValue());
                    // 更新库存，指定statement的ID和参数
                    sqlSession.update(DEDUCT_STOCK_STATEMENT, param);
                }
                // 刷新，提交SQL
                sqlSession.flushStatements();
            });
        } catch (Exception e) {
            throw new LyException(500, "库存不足！");
        }
    }
}
