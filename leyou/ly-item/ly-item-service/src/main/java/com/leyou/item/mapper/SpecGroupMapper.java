package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leyou.item.entity.SpecGroup;

/**
 * @author 虎哥
 */
public interface SpecGroupMapper extends BaseMapper<SpecGroup> {
}