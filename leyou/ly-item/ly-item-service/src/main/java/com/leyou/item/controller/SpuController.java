package com.leyou.item.controller;

import com.leyou.item.entity.Spu;
import com.leyou.item.service.SpuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8(Spu)表控制层
 *
 * @author makejava
 * @since 2020-09-15 16:37:21
 */
@RestController
@RequestMapping("spu")
public class SpuController {
    /**
     * 服务对象
     */
    @Resource
    private SpuService spuService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public Spu selectOne(@PathVariable("id") Long id) {
        return this.spuService.getById(id);
    }

}