package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.entity.CategoryBrand;

/**
 * @author 虎哥
 */
public interface CategoryBrandService extends IService<CategoryBrand> {
}
