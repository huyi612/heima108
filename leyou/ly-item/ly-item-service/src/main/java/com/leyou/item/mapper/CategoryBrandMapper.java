package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leyou.item.entity.CategoryBrand;

/**
 * @author 虎哥
 */
public interface CategoryBrandMapper extends BaseMapper<CategoryBrand> {
}
