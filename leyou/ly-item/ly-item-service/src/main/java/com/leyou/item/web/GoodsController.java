package com.leyou.item.web;

import com.leyou.common.dto.PageDTO;
import com.leyou.item.dto.SkuDTO;
import com.leyou.item.dto.SpecParamDTO;
import com.leyou.item.dto.SpuDTO;
import com.leyou.item.dto.SpuDetailDTO;
import com.leyou.item.entity.Sku;
import com.leyou.item.service.SkuService;
import com.leyou.item.service.SpuDetailService;
import com.leyou.item.service.SpuService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("goods")
public class GoodsController {

    private final SpuService spuService;

    private final SpuDetailService detailService;

    private final SkuService skuService;

    public GoodsController(SpuService SpuService, SpuDetailService detailService, SkuService skuService) {
        this.spuService = SpuService;
        this.detailService = detailService;
        this.skuService = skuService;
    }

    /**
     * 分页查询spu
     *
     * @return 分页结果
     */
    @GetMapping("/spu/page")
    public ResponseEntity<PageDTO<SpuDTO>> querySpuByPage(
            @RequestParam(value = "brandId", required = false) Long brandId,
            @RequestParam(value = "categoryId", required = false) Long categoryId,
            @RequestParam(value = "id", required = false) Long id,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "rows", defaultValue = "5") Integer rows,
            @RequestParam(value = "saleable", required = false) Boolean saleable
    ) {
        return ResponseEntity.ok(spuService.querySpuByPage(brandId, categoryId, id, page, rows, saleable));
    }

    /**
     * 新增商品
     *
     * @param spuDTO 商品数据
     * @return 无
     */
    @PostMapping
    public ResponseEntity<Void> saveGoods(@RequestBody SpuDTO spuDTO) {
        spuService.saveGoods(spuDTO);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 修改商品
     *
     * @param spuDTO 商品数据
     * @return 无
     */
    @PutMapping
    public ResponseEntity<Void> updateGoods(@RequestBody SpuDTO spuDTO) {
        spuService.updateGoods(spuDTO);
        return ResponseEntity.noContent().build();
    }

    /**
     * 更新商品的上下架
     *
     * @param spuId    商品spu的ID
     * @param saleable 上架或下架
     * @return 无
     */
    @PutMapping("saleable")
    public ResponseEntity<Void> updateSaleable(@RequestParam("id") Long spuId, @RequestParam("saleable") Boolean saleable) {
        spuService.updateSaleable(spuId, saleable);
        return ResponseEntity.noContent().build();
    }

    /**
     * 根据id查询商品的Spu、sku、spuDetail信息
     *
     * @param id 商品spu的id
     * @return 所有信息
     */
    @GetMapping("/{id}")
    public ResponseEntity<SpuDTO> queryGoodsById(@PathVariable("id") Long id) {
        SpuDTO spuDTO = spuService.queryGoodsById(id);
        return ResponseEntity.ok(spuDTO);
    }

    /**
     * 根据id批量查询sku
     *
     * @param ids id集合
     * @return sku集合
     */
    @GetMapping("/sku/list")
    public ResponseEntity<List<SkuDTO>> querySkuList(@RequestParam("ids") List<Long> ids) {
        List<Sku> skuList = skuService.listByIds(ids);
        List<SkuDTO> dtoList = SkuDTO.convertEntityList(skuList);
        return ResponseEntity.ok(dtoList);
    }

    /**
     * 根据spuId查询sku集合
     *
     * @param spuId spuId的集合
     * @return sku的集合
     */
    @GetMapping("/sku/of/spu")
    public ResponseEntity<List<SkuDTO>> querySkuBySpu(@RequestParam("id") Long spuId) {
        return ResponseEntity.ok(skuService.querySkuBySpuId(spuId));
    }

    /**
     * 根据spuId查询商品详情
     *
     * @param spuId spu的id
     * @return 商品详情
     */
    @GetMapping("/spu/detail")
    public ResponseEntity<SpuDetailDTO> queryDetailBySpu(@RequestParam("id") Long spuId) {
        return ResponseEntity.ok(detailService.queryDetailBySpuId(spuId));
    }

    /**
     * 根据商品id查询规格参数键值对
     *
     * @param spuId     商品id
     * @param searching 是否参与搜索
     * @return 规格参数集合
     */
    @GetMapping("/spec/value")
    public ResponseEntity<List<SpecParamDTO>> querySpecValueBySpuId(
            @RequestParam("id") Long spuId, @RequestParam(value = "searching", required = false) Boolean searching) {
        List<SpecParamDTO> list = spuService.querySpecValue(spuId, searching);
        return ResponseEntity.ok(list);
    }

    /**
     * 根据id查询spu
     * @param id 商品id
     * @return spu信息
     */
    @GetMapping("/spu/{id}")
    public ResponseEntity<SpuDTO> querySpuById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(new SpuDTO(spuService.getById(id)));
    }

    /**
     * 批量扣减库存
     * @param skuMap 商品及扣减数量
     */
    @PutMapping("/stock/minus")
    public ResponseEntity<Void> deductStock(@RequestBody Map<Long, Integer> skuMap){
        skuService.deductStock(skuMap);
        return ResponseEntity.noContent().build();
    }

    /**
     * 批量恢复库存
     * @param skuMap 商品及恢复数量
     */
    @PutMapping("/stock/plus")
    public ResponseEntity<Void> plusStock(@RequestBody Map<Long, Integer> skuMap){
        skuService.plusStock(skuMap);
        return ResponseEntity.noContent().build();
    }
}