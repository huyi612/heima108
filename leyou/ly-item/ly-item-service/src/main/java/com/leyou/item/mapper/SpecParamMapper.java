package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leyou.item.entity.SpecParam;

/**
 * @author 虎哥
 */
public interface SpecParamMapper extends BaseMapper<SpecParam> {
}