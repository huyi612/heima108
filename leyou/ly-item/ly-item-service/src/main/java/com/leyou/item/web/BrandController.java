package com.leyou.item.web;

import com.leyou.common.dto.PageDTO;
import com.leyou.item.dto.BrandDTO;
import com.leyou.item.service.BrandService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("brand")
public class BrandController {

    private final BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    /**
     * 根据id查询品牌
     * @param id 品牌的id
     * @return 品牌对象
     */
    @GetMapping("/{id}")
    public ResponseEntity<BrandDTO> queryBrandById(@PathVariable("id") Long id){
        return ResponseEntity.ok(new BrandDTO(brandService.getById(id)));
    }

    /**
     * 根据品牌id集合查询品牌集合
     * @param idList id集合
     * @return BrandDTO 集合
     */
    @GetMapping("/list")
    public ResponseEntity<List<BrandDTO>> queryBrandByIds(@RequestParam("ids") List<Long> idList){
        return ResponseEntity.ok(BrandDTO.convertEntityList(brandService.listByIds(idList)));
    }

    /**
     *
     * @param page 当前页码
     * @param rows 每页大小
     * @param key 查询条件
     * @return 品牌分页结果
     */
    @GetMapping("page")
    public ResponseEntity<PageDTO<BrandDTO>> queryBrandByPage(
            @RequestParam(value = "page", defaultValue = "1")Integer page,
            @RequestParam(value = "rows", defaultValue = "5")Integer rows,
            @RequestParam(value = "key", required = false)String key
    ){
        return ResponseEntity
                .ok(brandService.queryBrandByPage(page,rows, key));
    }

    /**
     * 新增品牌
     * @param brandDTO 品牌属性
     * @return 无
     */
    @PostMapping
    public ResponseEntity<Void> saveBrand(BrandDTO brandDTO){
        // 新增品牌
        brandService.saveBrand(brandDTO);
        // 新增成功，返回201
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 修改品牌
     * @param brandDTO 品牌及分类信息
     * @return 无
     */
    @PutMapping
    public ResponseEntity<Void> updateBrand(BrandDTO brandDTO) {
        brandService.updateBrand(brandDTO);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * 根据分类查询品牌
     * @return 品牌集合
     */
    @GetMapping("/of/category")
    public ResponseEntity<List<BrandDTO>> queryBrandByCategory(@RequestParam("id") Long id){
        return ResponseEntity.ok(brandService.queryBrandByCategory(id));
    }
}
