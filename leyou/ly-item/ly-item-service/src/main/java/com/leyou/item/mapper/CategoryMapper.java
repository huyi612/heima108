package com.leyou.item.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.leyou.item.entity.Category;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 虎哥
 */
public interface CategoryMapper extends BaseMapper<Category> {

    @Select("SELECT c.*\n" +
            "FROM tb_category_brand cb\n" +
            "INNER JOIN tb_category c ON c.id = cb.category_id\n" +
            "WHERE cb.brand_id = 43644")
    List<Category> queryByBrandId(Long brandId);
}
