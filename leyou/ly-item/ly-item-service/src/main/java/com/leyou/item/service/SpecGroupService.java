package com.leyou.item.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.leyou.item.dto.SpecGroupDTO;
import com.leyou.item.entity.SpecGroup;

import java.util.List;

/**
 * @author 虎哥
 */
public interface SpecGroupService extends IService<SpecGroup> {
    List<SpecGroupDTO> querySpecs(Long categoryId);
}