package com.leyou.item;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author 虎哥
 */
public class StreamTest {

    @Test
    public void name() {
        List<String> list = Arrays.asList("hello", "world", "java");


        // java 不支持函数式编程

        // 也就是说函数无法作为参数传递，为了能够传递函数，不得不把函数封装到一个对象中传递，但是写法太丑了

        list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println("s = " + s);
            }
        });
        // 于是java8就给出了一个语法糖：Lambda表达式，简化书写

        list.forEach(s -> {
            System.out.println("s = " + s);
        });

        // 如果一个接口，其中只有一个方法，称为函数式接口，Lambda表达式只能简化函数式接口，
        // 为了方便，JDK定义了很多的专门的函数式接口，大概分为四类：
        // - Function: 有参有返回值
        // - Consumer: 有参无返回值
        // - Supplier: 无参有返回值
        // - Runnable: 无参无返回值
        // - Predicate: 有参，返回值为boolean


        // 上面代码中，我们把System.out.println封装成一个新的consumer，作为参数传递
        // 但是，我们发现 System.out.println 自己就是有参无返回值，符合Consumer的规范，
        // 那么，我们为什么要再封装Consumer，而不是直接把System.out.println作为Consumer传递呢？
        // 于是，JDK8 就引入了方法引用的概念，可以把一个服务函数式接口定义的方法，作为参数传递
        // 其语法是：
        // - 对象实例::方法名  ，只能用在方法是一个成员方法，方法的参数刚好是集合中的元素， 例如
        list.forEach(System.out::println);

        // - 类名::方法名   ， 只能用在方法是一个静态方法，方法的参数刚好是集合中的元素
        list.forEach(StringUtils::upperCase);

        // - 类名::方法名   ， 只能用在方法是一个成员方法，方法的调用者刚好是集合中的元素
        list.forEach(String::length);

        // - 类名::new  , 只能用在要传递的方法是一个构造函数方法，方法的参数刚好是集合中的元素
    }

    @Test
    public void test2() {
        List<String> stringList = Arrays.asList("-1", "10", "5", "15", "2");

        List<Integer> list = stringList
                .stream()
                .map(Integer::parseInt)
                .map(Math::abs)
                .filter(num -> num % 2 == 1)
                .collect(Collectors.toList());

        list.forEach(System.out::println);
    }
}
