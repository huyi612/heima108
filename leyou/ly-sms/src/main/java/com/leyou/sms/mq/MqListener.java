package com.leyou.sms.mq;

import com.leyou.common.utils.RegexUtils;
import com.leyou.sms.util.SmsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Map;

import static com.leyou.common.constants.MQConstants.ExchangeConstants.SMS_EXCHANGE_NAME;
import static com.leyou.common.constants.MQConstants.QueueConstants.SMS_VERIFY_CODE_QUEUE;
import static com.leyou.common.constants.MQConstants.RoutingKeyConstants.VERIFY_CODE_KEY;

/**
 * @author 虎哥
 */
@Slf4j
@Component
public class MqListener {

    @Autowired
    private SmsUtil smsUtil;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = SMS_VERIFY_CODE_QUEUE, durable = "true"),
            exchange = @Exchange(name = SMS_EXCHANGE_NAME, type = ExchangeTypes.TOPIC),
            key = VERIFY_CODE_KEY
    ))
    public void listenVerifyCodeMessage(Map<String, String> msg) {
        if (CollectionUtils.isEmpty(msg)) {
            // 消息为空，直接放弃消息
            return;
        }
        // 获取手机号
        String phone = msg.get("phone");
        if (!RegexUtils.isPhone(phone)) {
            // 手机号格式错误，直接放弃消息
            log.error("发送短信的手机号格式错误， phone: {}", phone);
            return;
        }
        // 获取code
        String code = msg.get("code");
        if (!RegexUtils.isCodeValid(code)) {
            // code格式错误，直接放弃消息
            log.error("发送短信的手机号格式错误， code: {}", code);
            return;
        }
        // 发送短信
        smsUtil.sendVerifyCode(phone, code);
    }
}
