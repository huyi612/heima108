package cn.itcast.demo.entity;

import java.util.Date;
import java.util.concurrent.DelayQueue;

/**
 * @author 虎哥
 */
public class DelayQueueDemo {
    public static void main(String[] args) throws InterruptedException {
        // 创建一个延迟队列
        final DelayQueue<DelayTask> queue = new DelayQueue<DelayTask>();
        // 往队列中添加元素
        System.out.println("开始添加任务 = " + new Date());
        queue.add(new DelayTask("orderId=110", 10));
        queue.add(new DelayTask("orderId=112", 5));
        System.out.println("添加任务完毕 = " + new Date());
        // 尝试从队列中取出元素
        while (!queue.isEmpty()){

            DelayTask task = queue.take();
            System.out.println("取出任务 = " + task);
            System.out.println("时间 = " + new Date());
        }

    }
}
