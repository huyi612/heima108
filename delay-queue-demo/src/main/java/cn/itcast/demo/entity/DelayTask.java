package cn.itcast.demo.entity;

import lombok.Data;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author 虎哥
 */
@Data
public class DelayTask implements Delayed {
    /**
     * 延迟任务的数据，本例中，可以是订单的id
     */
    private String data;
    private long deadLine;

    public DelayTask(String data, long remainTime) {
        this.data = data;
        this.deadLine = System.currentTimeMillis() + remainTime * 1000;
    }

    /**
     * 获取剩余有效期
     * @param unit 要转换的目标时间单位
     * @return 以参数unit为单位的时间值
     */
    public long getDelay(TimeUnit unit) {
        return unit.convert(this.deadLine - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    /**
     * 排序
     * @param o 要比较的另一个延时任务
     * @return 0，相等，-1，小于。 1 大于
     */
    public int compareTo(Delayed o) {
        long n = this.getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS);
        return n == 0 ? 0 : (n > 0 ? 1 : -1);
    }
}
