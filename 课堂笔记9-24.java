
$2a$06$LNkVYCLlIp1cvzgMT.7XG.yPatpVJLOGgVqHEAX7RRsjsLAOoPObi
$2a$06$LNkVYCLlIp1cvzgMT.7XG.yPatpVJLOGgVqHEAX7RRsjsLAOoPObi

public void sendMessage(String phone){}






// 登录业务
login(){
	- 校验用户名密码
	- 如果成功，写入session
	- 发送短信，通知用户，您已经在xx登录了！
	sendMessage()
}

// 订单业务
createOrder(){
	- 用户下单
	- 用户付款
	- 发送短信：您的订单已经发货
	sendMessage()
}


// 发送短信的代码：
	- 1.接收前端请求参数中的手机号，并验证
	- 2.生成随即验证码
	- 3.保存验证码到redis
	- 4.利用AmqpTemplate，发送消息到ly-sms
	
	
// 用户注册
  - 1.校验验证码是否正确
  - 2.验证用户其它数据
  - 3.密码加密
  - 4.把用户写入数据库
	
	S	R1	R2	R3	R4	R5	R6	R7
 
123 -S-> a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae1 -R1->  
321 -s-> a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae2  -R2-> 
213 -s-> a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3

// 登录逻辑
	- 校验用户名密码
	- 如果成功，把用户写入session
 
// 查询用户
 - 校验用户名密码
 - 返回用户信息
 