package com.github.wxpay.test;

import com.github.wxpay.sdk.*;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 虎哥
 */
public class PayTest {

    private WXPay wxPay;

    @Before
    public void setup() throws Exception{
        // 准备WXPay工具，主要准备AppID、MchID、密钥（签名）
        WXPayConfigImpl payConfig = new WXPayConfigImpl();
        payConfig.setAppID("wx8397f8696b538317");
        payConfig.setMchID("1473426802");
        payConfig.setKey("T6m9iK73b0kn9g5v426MKfHQH7X8rKwb");
        // 创建wxPay工具
        this.wxPay = new WXPay(payConfig, "http://api.leyou.com/trade/pay");
    }

    /**
     * 统一下单
     */
    @Test
    public void testUnifiedOrder(){
        // 5个请求参数：
        Map<String, String> data = new HashMap<String, String>();
        // 商品描述
        data.put("body", "腾讯充值中心-QQ会员充值");
        // 订单编号
        data.put("out_trade_no", "2034414151012");
        // 交易金额
        data.put("total_fee", "1");
        // 终端IP
        data.put("spbill_create_ip", "123.12.12.123");
        // 交易类型
        data.put("trade_type", "NATIVE");

        try {
            // 下单
            Map<String, String> resp = wxPay.unifiedOrder(data);
            System.out.println(resp);

            boolean isValid = WXPayUtil.isSignatureValid(resp, "T6m9iK73b0kn9g5v426MKfHQH7X8rKwb", WXPayConstants.SignType.HMACSHA256);

            System.out.println("isValid = " + isValid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
