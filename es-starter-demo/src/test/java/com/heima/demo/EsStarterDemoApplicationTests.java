package com.heima.demo;

import cn.itcast.starter.es.util.ElasticClient;
import com.heima.demo.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EsStarterDemoApplicationTests {

    @Autowired
    private ElasticClient elasticClient;

    @Test
    public void contextLoads() throws IOException {
        User user = elasticClient.getById("1", User.class);
        System.out.println("user = " + user);
    }

    @Test
    public void test2() throws IllegalAccessException, NoSuchFieldException, IOException {
        List<User> list = elasticClient.searchHighlight("note", "java", User.class);

        list.forEach(System.out::println);
    }
}
