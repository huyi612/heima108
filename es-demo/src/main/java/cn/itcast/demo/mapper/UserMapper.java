package cn.itcast.demo.mapper;


import cn.itcast.demo.entity.User;

import java.util.List;

/**
 * @author 虎哥
 */
public interface UserMapper {

    User findById(Long id);

    List<User> findAll();
}