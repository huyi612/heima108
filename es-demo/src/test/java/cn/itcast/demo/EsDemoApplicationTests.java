package cn.itcast.demo;

import cn.itcast.demo.entity.User;
import cn.itcast.demo.service.UserService;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.MainResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class EsDemoApplicationTests {

    private UserService userService;

    private RestHighLevelClient client;

    @Test
    public void testInit() throws IOException {
        MainResponse info = client.info(RequestOptions.DEFAULT);

        System.out.println("info = " + info);
    }

    @Test
    public void testCreateIndex() throws IOException {
        // new一个创建索引库的请求对象
        CreateIndexRequest request = new CreateIndexRequest("user");
        // 配置settings
        request.settings("{\n" +
                "    \"number_of_shards\": 3,\n" +
                "    \"number_of_replicas\": 1\n" +
                "  }", XContentType.JSON);
        // 配置mappings
        request.mapping("{\n" +
                "    \"properties\": {\n" +
                "      \"id\": {\n" +
                "        \"type\": \"keyword\"\n" +
                "      },\n" +
                "      \"note\": {\n" +
                "        \"type\": \"text\",\n" +
                "        \"analyzer\": \"ik_smart\"\n" +
                "      },\n" +
                "      \"name\": {\n" +
                "        \"type\": \"keyword\"\n" +
                "      },\n" +
                "      \"age\": {\n" +
                "        \"type\": \"integer\"\n" +
                "      },\n" +
                "      \"gender\": {\n" +
                "        \"type\": \"keyword\"\n" +
                "      }\n" +
                "    }\n" +
                "  }", XContentType.JSON);
        // 发请求
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);

        System.out.println("createIndexResponse = " + createIndexResponse);
    }

    @Test
    public void testDeleteIndex() throws IOException {
        // new一个创建索引库的请求对象
        DeleteIndexRequest request = new DeleteIndexRequest("user");
        // 发请求
        AcknowledgedResponse deleteIndexResponse = client.indices().delete(request, RequestOptions.DEFAULT);

        System.out.println("deleteIndexResponse = " + deleteIndexResponse);
    }

    @Test
    public void addDocument() throws IOException {
        // 准备文档数据
        User user = userService.findById(1L);
        user.setAge(32);
        // 1.new 一个新增文档的请求对象
        IndexRequest request = new IndexRequest("user")
                // 2.准备请求参数
                .id(user.getId().toString()) // id参数
                .source(JSON.toJSONString(user), XContentType.JSON);// 文档的json数据

        // 3.发请求
        IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);

        System.out.println("indexResponse = " + indexResponse);
    }

    @Test
    public void testGetDocument() throws IOException {
        // 1.new 一个查询文档的请求对象
        GetRequest request = new GetRequest("user", "1");
        // 2.发请求
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        // 3.解析请求
        String json = response.getSourceAsString();
        User user = JSON.parseObject(json, User.class);
        System.out.println("user = " + user);
    }

    @Test
    public void testDeleteDocument() throws IOException {
        // 1.new 一个删除文档的请求对象
        DeleteRequest request = new DeleteRequest("user", "1");
        // 2.发请求
        DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        // 3.解析请求
        System.out.println("response = " + response);
    }

    @Test
    public void testUpdate() throws IOException {
        // 1.new 一个请求对象
        UpdateRequest request = new UpdateRequest("user", "1");
        // 2.准备请求参数
        request.doc("{\"age\": 34}", XContentType.JSON);
        // 3.发请求
        UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
        System.out.println("response = " + response);
    }

    @Test
    public void testBulk() throws IOException {
        // 0.查询文档
        List<User> list = userService.findAll();

        // 1.new 一个请求对象
        BulkRequest request = new BulkRequest("user");
        // 2.准备请求参数
        for (User user : list) {
            request.add(
                    new IndexRequest("user").id(user.getId().toString())
                            .source(JSON.toJSONString(user), XContentType.JSON));
        }

        // 3.发请求
        BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
        System.out.println("response = " + response);
    }

    @Before
    public void init() {
        userService = new UserService();
        this.client = new RestHighLevelClient(
                RestClient.builder(
                        HttpHost.create("http://localhost:9200"),
                        HttpHost.create("http://localhost:9201"),
                        HttpHost.create("http://localhost:9202")
                )
        );
    }

    @After
    public void close() throws IOException {
        client.close();
    }

    @Test
    public void contextLoads() {
        List<User> list = userService.findAll();

        for (User user : list) {
            System.out.println("user = " + user);
        }
    }

}
