package cn.itcast.demo;

import cn.itcast.demo.entity.User;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

public class EsSearchTests {

    private RestHighLevelClient client;

    @Test
    public void testMatchSearch() throws IOException {
        // 1.准备搜索条件对象，就是搜索时参数的JSON，包含：query、sort、from、size、highlight等
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query条件
        searchSourceBuilder.query(QueryBuilders.matchQuery("note", "java"));
        // 1.2.sort
        // 1.3.from和size分页
        // 1.4.高亮
        // 1.5.source过滤

        // 2.搜索
        searchWithSourceBuilder(searchSourceBuilder);
    }

    @Test
    public void testTermSearch() throws IOException {
        // 1.准备搜索条件对象，就是搜索时参数的JSON，包含：query、sort、from、size、highlight等
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query条件
        searchSourceBuilder.query(QueryBuilders.termQuery("gender", "女"));
        // 2.搜索
        searchWithSourceBuilder(searchSourceBuilder);
    }

    @Test
    public void testRangeSearch() throws IOException {
        // 1.准备搜索条件对象，就是搜索时参数的JSON，包含：query、sort、from、size、highlight等
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query条件
        searchSourceBuilder.query(QueryBuilders.rangeQuery("age").gte(18).lte(25));
        // 2.搜索
        searchWithSourceBuilder(searchSourceBuilder);
    }

    @Test
    public void testBoolSearch() throws IOException {
        // 1.准备搜索条件对象，就是搜索时参数的JSON，包含：query、sort、from、size、highlight等
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query条件
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        boolQuery.must(QueryBuilders.matchQuery("note", "jave").fuzziness(1));
        boolQuery.filter(QueryBuilders.rangeQuery("age").gte(18).lte(25));

        searchSourceBuilder.query(boolQuery);
        // 2.搜索
        searchWithSourceBuilder(searchSourceBuilder);
    }

    @Test
    public void testOtherSearch() throws IOException {
        int page = 2;
        int size = 3;
        // 1.准备搜索条件对象，就是搜索时参数的JSON，包含：query、sort、from、size、highlight等
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query条件
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        // 1.2.排序
        searchSourceBuilder.sort("age", SortOrder.ASC);
        // 1.3.分页
        searchSourceBuilder.from((page - 1) * size).size(size);
        // 1.4.source过滤
        searchSourceBuilder.fetchSource(new String[]{"name", "age"}, null);
        // 2.搜索
        searchWithSourceBuilder(searchSourceBuilder);
    }

    @Test
    public void testHighlight() throws IOException {
        // 1.准备搜索参数对象
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 1.1.query
        searchSourceBuilder.query(QueryBuilders.matchQuery("note", "java"));
        // 1.2.高亮
        searchSourceBuilder.highlighter(new HighlightBuilder().field("note"));

        // 2.准备搜索请求对象
        SearchRequest request = new SearchRequest("user");
        // 3.准备请求参数
        request.source(searchSourceBuilder);
        // 4.发请求
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        // 5.解析
        SearchHits searchHits = response.getHits();
        // 5.1.总条数
        long total = searchHits.getTotalHits().value;
        System.out.println("本次共搜索到" + total + "条数据：");
        // 5.2.获取数据
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            // 5.3.解析source部分数据并反序列化为对象
            String json = hit.getSourceAsString();
            User user = JSON.parseObject(json, User.class);
            // 5.4.获取高亮部分
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField field = highlightFields.get("note");
            // 拼接高亮字段
            String note = StringUtils.join(field.getFragments());
            user.setNote(note);

            System.out.println("user = " + user);
        }
    }

    private void searchWithSourceBuilder(SearchSourceBuilder searchSourceBuilder) throws IOException {
        // 2.new了一个搜索请求对象
        SearchRequest searchRequest = new SearchRequest("user");
        // 3.准备请求参数
        searchRequest.source(searchSourceBuilder);
        // 4.发请求
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        // 5.解析响应
        SearchHits searchHits = response.getHits();
        // 5.1.总条数
        long total = searchHits.getTotalHits().value;
        System.out.println("本次共搜索到" + total + "条数据：");
        // 5.2.获取数据
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            String json = hit.getSourceAsString();
            User user = JSON.parseObject(json, User.class);
            System.out.println("user = " + user);
        }
    }

    @Before
    public void init() {
        this.client = new RestHighLevelClient(
                RestClient.builder(
                        HttpHost.create("http://localhost:9200"),
                        HttpHost.create("http://localhost:9201"),
                        HttpHost.create("http://localhost:9202")
                )
        );
    }

    @After
    public void close() throws IOException {
        client.close();
    }
}
