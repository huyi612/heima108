package cn.itcast.consumer.client;

import cn.itcast.consumer.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author 虎哥
 */
@Component
public class UserClientFallback implements UserClient{
    @Override
    public User findById(Long userId) {
        User user = new User();
        user.setName("测试，备选的用户");
        return user;
    }
}
