package cn.itcast.consumer.client;

import cn.itcast.consumer.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Feign其实是在帮我们发送一个http的请求。
 * 本例中查询用户，真实请求地址是：GET http://user-service/user/2
 * Feign帮我们发送这个请求，需要知道请求中的一些信息：
 *  - 请求方式: GET --> 通过@GetMapping得到
 *  - 请求路径: /user/{id} --> 通过@GetMapping("/user/{id}")得到
 *  - 请求参数: Long id  --> 通过@PathVariable("id") Long id得到
 *  - 返回值类型: User   --> 方法的返回值
 *  - 服务名称: user-service    --> 通过接口上的注解：@FeignClient("user-service")
 *
 * Feign会在项目启动时，读取接口的字节码信息，获取注解信息，获取注解中的上述内容。
 * 对接口动态代理，实现其中的方法，在方法中帮我们发送http的请求
 * @author 虎哥
 */
@FeignClient(value = "user-service", fallback = UserClientFallback.class)
public interface UserClient {

    @GetMapping("/user/{id}")
    User findById(@PathVariable("id") Long userId);
}
