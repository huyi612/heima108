package cn.itcast.gateway.limit;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author 虎哥
 */
@Component
public class IpKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        // 获取用户IP
        String hostName = exchange.getRequest().getRemoteAddress().getHostName();
        // 生成限流令牌桶的key
        return Mono.just("limit:" + hostName);
    }
}
