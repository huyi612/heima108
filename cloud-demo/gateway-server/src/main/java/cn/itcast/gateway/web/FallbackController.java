package cn.itcast.gateway.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 虎哥
 */
@RestController
@RequestMapping("/fallback")
public class FallbackController {

    @RequestMapping("/handler")
    public Mono<Map<String, Object>> handleFallback(){
        return Mono.fromSupplier( () -> {
            Map<String, Object> result = new HashMap<>();
            result.put("status", 504);
            result.put("msg", "请求超时，请稍后再试！");
            return result;
        });
    }
}
