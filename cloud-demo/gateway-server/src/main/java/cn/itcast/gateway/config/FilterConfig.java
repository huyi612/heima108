package cn.itcast.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import reactor.core.publisher.Mono;

/**
 * @author 虎哥
 */
@Slf4j
@Configuration
public class FilterConfig {

    @Bean
    @Order(1)
    public GlobalFilter globalFilter1(){
        return (exchange, chain) -> {
            // pre逻辑
            log.warn("过滤器1的pre阶段");
            return chain.filter(exchange). // post逻辑
                    then(Mono.fromRunnable(() -> log.warn("过滤器1的post阶段")));
        };
    }
    @Bean
    @Order(2)
    public GlobalFilter globalFilter2(){
        return (exchange, chain) -> {
            // pre逻辑
            log.warn("过滤器2的pre阶段");
            return chain.filter(exchange). // post逻辑
                    then(Mono.fromRunnable(() -> log.warn("过滤器2的post阶段")));
        };
    }
    @Bean
    @Order(3)
    public GlobalFilter globalFilter3(){
        return (exchange, chain) -> {
            // pre逻辑
            log.warn("过滤器3的pre阶段");
            return chain.filter(exchange). // post逻辑
                    then(Mono.fromRunnable(() -> log.warn("过滤器3的post阶段")));
        };
    }
}
